<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20211007070331 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE preferences_preferences CHANGE is_deleted is_deleted TINYINT(1) NOT NULL COMMENT \'Предложение удалено пользователем как не релевантное\', CHANGE is_conversioned is_conversioned TINYINT(1) NOT NULL COMMENT \'Просмотрено\', CHANGE is_liked is_liked TINYINT(1) NOT NULL COMMENT \'Понравилось\', CHANGE is_purchased is_purchased TINYINT(1) NOT NULL COMMENT \'Куплено\'');
        $this->addSql('ALTER TABLE preferences_survey_result CHANGE processing_date_time processing_date_time DATETIME DEFAULT NULL COMMENT \'Дата и время обработки результатов опроса\'');
        $this->addSql('ALTER TABLE preferences.preferences_preferences CHANGE turproduct_id tourproduct_id bigint unsigned NOT NULL COMMENT \'Рекомендуемый продукт\'');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE preferences_preferences CHANGE is_deleted is_deleted SMALLINT UNSIGNED DEFAULT 0 NOT NULL COMMENT \'Предложение удалено пользователем как не релевантное\', CHANGE is_conversioned is_conversioned SMALLINT UNSIGNED DEFAULT 0 NOT NULL COMMENT \'Просмотрено\', CHANGE is_liked is_liked SMALLINT UNSIGNED DEFAULT 0 NOT NULL COMMENT \'Понравилось\', CHANGE is_purchased is_purchased SMALLINT UNSIGNED DEFAULT 0 NOT NULL COMMENT \'Куплено\'');
        $this->addSql('ALTER TABLE preferences_survey_result CHANGE processing_date_time processing_date_time DATETIME DEFAULT NULL COMMENT \'Дата и время обработки результатов опроса\'');
        $this->addSql('ALTER TABLE preferences.preferences_preferences CHANGE tourproduct_id turproduct_id bigint unsigned NOT NULL COMMENT \'Рекомендуемый продукт\'');
    }
}
