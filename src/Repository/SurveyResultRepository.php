<?php

namespace App\Repository;

use App\Entity\SurveyResult;
use DateTime;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;
use Doctrine\ORM\Tools\Pagination\Paginator;

/**
 * @method SurveyResult|null find($id, $lockMode = null, $lockVersion = null)
 * @method SurveyResult|null findOneBy(array $criteria, array $orderBy = null)
 * @method SurveyResult[]    findAll()
 * @method SurveyResult[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class SurveyResultRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, SurveyResult::class);
    }

    /**
     * @return SurveyResult
     * @throws \Doctrine\ORM\ORMException
     */
    public function create(): SurveyResult
    {
        $surveyResult=new SurveyResult();
        $this->getEntityManager()->persist($surveyResult);
        return $surveyResult;
    }

    /**
     * @param SurveyResult $surveyResult
     *
     * @throws \Doctrine\ORM\ORMException
     */
    public function remove(SurveyResult $surveyResult)
    {
        $this->getEntityManager()->remove($surveyResult);
    }

    /**
     * @param array $params
     * @param array|null $orderBy
     * @param null $limit
     * @param null $offset
     * @param int $total
     * @return SurveyResult[]
     */
    public function search(array $params, array $orderBy = null, $limit = null, $offset = null, &$total=0): array
    {
        $qb=$this->createQueryBuilder("t");

        if (isset($params["id"]))
        {
            $qb->andWhere("t.id = :id")->setParameter("id", $params["id"]);
        }

        if (isset($params["surveyId"]))
        {
            $qb->andWhere("t.survey = :surveyId")->setParameter("surveyId", $params["surveyId"]);
        }

        if (isset($params["customerId"]))
        {
            $qb->andWhere("t.customerId = :customerId")->setParameter("customerId", $params["customerId"]);
        }

        if (isset($params["status"]))
        {
            $qb->andWhere("t.status = :status")->setParameter("status", $params["status"]);
        }

        if (isset($params["surveyDate"]))
        {
            /* @var DateTime $from */
            $from = $params["surveyDate"];
            $to = clone $from;
            $to->add(new \DateInterval('P1D'));
            $qb->andWhere('t.surveyDateTime >= :from')
                ->andWhere('t.surveyDateTime < :to')
            ->setParameter('from', $from->format("Y-m-d") )
            ->setParameter('to', $to->format("Y-m-d"));
        }

        if (isset($orderBy))
        {
            if (is_array($orderBy))
            {
                foreach ($orderBy as $sort=>$order)
                {
                    $qb->addOrderBy(is_int($sort)?("t.".$order):("t.".$sort),is_int($sort)?null:$order);
                }
            }
        }
        $qb->setFirstResult($offset)->setMaxResults($limit);
        $query=$qb->getQuery();
        $paginator = new Paginator($query);
        $total=count($paginator);
        return $query->getResult();
    }
}
