<?php

namespace App\Repository;

use App\Entity\SurveyQuestion;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method SurveyQuestion|null find($id, $lockMode = null, $lockVersion = null)
 * @method SurveyQuestion|null findOneBy(array $criteria, array $orderBy = null)
 * @method SurveyQuestion[]    findAll()
 * @method SurveyQuestion[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class SurveyQuestionRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, SurveyQuestion::class);
    }

    /**
     * @return SurveyQuestion
     * @throws \Doctrine\ORM\ORMException
     */
    public function create(): SurveyQuestion
    {
        $surveyQuestion=new SurveyQuestion();
        $this->getEntityManager()->persist($surveyQuestion);
        return $surveyQuestion;
    }

    /**
     * @param SurveyQuestion $surveyQuestion
     *
     * @throws \Doctrine\ORM\ORMException
     */
    public function remove(SurveyQuestion $surveyQuestion)
    {
        $this->getEntityManager()->remove($surveyQuestion);
    }

}
