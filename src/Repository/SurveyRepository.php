<?php

namespace App\Repository;

use App\Entity\Survey;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;
use Doctrine\ORM\Tools\Pagination\Paginator;

/**
 * @method Survey|null find($id, $lockMode = null, $lockVersion = null)
 * @method Survey|null findOneBy(array $criteria, array $orderBy = null)
 * @method Survey[]    findAll()
 * @method Survey[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class SurveyRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Survey::class);
    }

    /**
     * @return Survey
     * @throws \Doctrine\ORM\ORMException
     */
    public function create(): Survey
    {
        $survey=new Survey();
        $this->getEntityManager()->persist($survey);
        return $survey;
    }

    /**
     * @param Survey $survey
     *
     * @throws \Doctrine\ORM\ORMException
     */
    public function remove(Survey $survey)
    {
        $this->getEntityManager()->remove($survey);
    }


    /**
     * @param array $params
     * @param array|null $orderBy
     * @param null $limit
     * @param null $offset
     * @param int $total
     * @return Survey[]
     */
    public function search(array $params, array $orderBy = null, $limit = null, $offset = null, &$total=0): array
    {
        $qb=$this->createQueryBuilder("t");

        if (isset($params["id"]))
        {
            $qb->andWhere("t.id = :id")->setParameter("id", $params["id"]);
        }

        if (isset($params["title"]))
        {
            $qb->andWhere("t.title like :title")->setParameter("title", "%".$params["title"]."%");
        }

        if (isset($params["status"]))
        {
            $qb->andWhere("t.status = :status")->setParameter("status", $params["status"]);
        }

        if (isset($params["isRequired"]))
        {
            $qb->andWhere("t.isRequired = :isRequired")->setParameter("isRequired", $params["isRequired"]);
        }
        if (isset($params["repeatDays"]))
        {
            $qb->andWhere("t.repeatDays = :repeatDays")->setParameter("repeatDays", $params["repeatDays"]);
        }

        if (isset($orderBy))
        {
            if (is_array($orderBy))
            {
                foreach ($orderBy as $sort=>$order)
                {
                    $qb->addOrderBy(is_int($sort)?("t.".$order):("t.".$sort),is_int($sort)?null:$order);
                }
            }
        }
        $qb->setFirstResult($offset)->setMaxResults($limit);
        $query=$qb->getQuery();
        $paginator = new Paginator($query);
        $total=count($paginator);
        return $query->getResult();
    }

}
