<?php

namespace App\Repository;

use App\Entity\Preferences;
use DateInterval;
use DateTime;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;
use Doctrine\ORM\Tools\Pagination\Paginator;

/**
 * @method Preferences|null find($id, $lockMode = null, $lockVersion = null)
 * @method Preferences|null findOneBy(array $criteria, array $orderBy = null)
 * @method Preferences[]    findAll()
 * @method Preferences[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class PreferencesRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Preferences::class);
    }

    /**
     * @param array $params
     * @param array|null $orderBy
     * @param null $limit
     * @param null $offset
     * @param int $total
     * @return Preferences[]
     */
    public function search(array $params, array $orderBy = null, $limit = null, $offset = null, &$total=0): array
    {
        $qb=$this->createQueryBuilder("t");

        if (isset($params["id"]))
        {
            $qb->andWhere("t.id = :id")->setParameter("id", $params["id"]);
        }

        if (isset($params["preferencesRuleId"]))
        {
            $qb->andWhere("t.preferencesRule = :preferencesRuleId")->setParameter("preferencesRuleId", $params["preferencesRuleId"]);
        }

        if (isset($params["customerId"]))
        {
            $qb->andWhere("t.customerId = :customerId")->setParameter("customerId", $params["customerId"]);
        }

        if (isset($params["date"]))
        {
            $qb->andWhere("t.date = :date")->setParameter("date", $params["date"]);
        }

        if (isset($params["tourproductId"]))
        {
            $qb->andWhere("t.tourproductId = :tourproductId")->setParameter("tourproductId", $params["tourproductId"]);
        }

        if (isset($params["isDeleted"]))
        {
            $qb->andWhere("t.isDeleted = :isDeleted")->setParameter("isDeleted", $params["isDeleted"]);
        }

        if (isset($params["isConversioned"]))
        {
            $qb->andWhere("t.isConversioned = :isConversioned")->setParameter("isConversioned", $params["isConversioned"]);
        }

        if (isset($params["isLiked"]))
        {
            $qb->andWhere("t.isLiked = :isLiked")->setParameter("isLiked", $params["isLiked"]);
        }
        if (isset($params["isPurchased"]))
        {
            $qb->andWhere("t.isPurchased = :isPurchased")->setParameter("isPurchased", $params["isPurchased"]);
        }

        if (isset($orderBy))
        {
            if (is_array($orderBy))
            {
                foreach ($orderBy as $sort=>$order)
                {
                    $qb->addOrderBy(is_int($sort)?("t.".$order):("t.".$sort),is_int($sort)?null:$order);
                }
            }
        }
        $qb->setFirstResult($offset)->setMaxResults($limit);
        $query=$qb->getQuery();
        $paginator = new Paginator($query);
        $total=count($paginator);
        return $query->getResult();
    }
}
