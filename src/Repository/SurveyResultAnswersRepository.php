<?php

namespace App\Repository;

use App\Entity\SurveyResultAnswers;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method SurveyResultAnswers|null find($id, $lockMode = null, $lockVersion = null)
 * @method SurveyResultAnswers|null findOneBy(array $criteria, array $orderBy = null)
 * @method SurveyResultAnswers[]    findAll()
 * @method SurveyResultAnswers[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class SurveyResultAnswersRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, SurveyResultAnswers::class);
    }

    /**
     * @return SurveyResultAnswers
     * @throws \Doctrine\ORM\ORMException
     */
    public function create(): SurveyResultAnswers
    {
        $surveyResultAnswer=new SurveyResultAnswers();
        $this->getEntityManager()->persist($surveyResultAnswer);
        return $surveyResultAnswer;
    }

    /**
     * @param SurveyResultAnswers $surveyResultAnswer
     *
     * @throws \Doctrine\ORM\ORMException
     */
    public function remove(SurveyResultAnswers $surveyResultAnswer)
    {
        $this->getEntityManager()->remove($surveyResultAnswer);
    }
}
