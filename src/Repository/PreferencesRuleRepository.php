<?php

namespace App\Repository;

use App\Entity\PreferencesRule;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\Tools\Pagination\Paginator;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method PreferencesRule|null find($id, $lockMode = null, $lockVersion = null)
 * @method PreferencesRule|null findOneBy(array $criteria, array $orderBy = null)
 * @method PreferencesRule[]    findAll()
 * @method PreferencesRule[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class PreferencesRuleRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, PreferencesRule::class);
    }

    /**
     * @return PreferencesRule
     * @throws \Doctrine\ORM\ORMException
     */
    public function create(): PreferencesRule
    {
        $rule=new PreferencesRule();
        $this->getEntityManager()->persist($rule);
        return $rule;
    }

    /**
     * @param PreferencesRule $rule
     *
     * @throws \Doctrine\ORM\ORMException
     */
    public function remove(PreferencesRule $rule)
    {
        $this->getEntityManager()->remove($rule);
    }


    /**
     * @param array $params
     * @param array|null $orderBy
     * @param null $limit
     * @param null $offset
     * @param int $total
     * @return PreferencesRule[]
     */
    public function search(array $params, array $orderBy = null, $limit = null, $offset = null, &$total=0): array
    {
        $qb=$this->createQueryBuilder("t");

        if (isset($params["id"]))
        {
            $qb->andWhere("t.id = :id")->setParameter("id", $params["id"]);
        }

        if (isset($params["status"]))
        {
            $qb->andWhere("t.status = :status")->setParameter("status", $params["status"]);
        }

        if (isset($params["type"]))
        {
            $qb->andWhere("t.type = :type")->setParameter("type", $params["type"]);
        }

        if (isset($params["showType"]))
        {
            $qb->andWhere("t.showType = :showType")->setParameter("showType", $params["showType"]);
        }

        if (isset($params["isActive"]))
        {
            $qb->andWhere("t.isActive = :isActive")->setParameter("isActive", $params["isActive"]);
        }

        if (isset($params["isFilter"]))
        {
            $qb->andWhere("t.isFilter = :isFilter")->setParameter("isFilter", $params["isFilter"]);
        }

        if (isset($params["isStop"]))
        {
            $qb->andWhere("t.isStop = :isStop")->setParameter("isStop", $params["isStop"]);
        }

        if (isset($orderBy))
        {
            if (is_array($orderBy))
            {
                foreach ($orderBy as $sort=>$order)
                {
                    $qb->addOrderBy(is_int($sort)?("t.".$order):("t.".$sort),is_int($sort)?null:$order);
                }
            }
        }
        $qb->setFirstResult($offset)->setMaxResults($limit);
        $query=$qb->getQuery();
        $paginator = new Paginator($query);
        $total=count($paginator);
        return $query->getResult();
    }

}
