<?php

namespace App\Repository;

use App\Entity\PreferencesRuleStat;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method PreferencesRuleStat|null find($id, $lockMode = null, $lockVersion = null)
 * @method PreferencesRuleStat|null findOneBy(array $criteria, array $orderBy = null)
 * @method PreferencesRuleStat[]    findAll()
 * @method PreferencesRuleStat[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class PreferencesRuleStatRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, PreferencesRuleStat::class);
    }

    // /**
    //  * @return PreferencesRuleStat[] Returns an array of PreferencesRuleStat objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('p.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?PreferencesRuleStat
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
