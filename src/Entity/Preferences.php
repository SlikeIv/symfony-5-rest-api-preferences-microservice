<?php

namespace App\Entity;

use App\Repository\PreferencesRepository;
use DateTime;
use Doctrine\ORM\Mapping as ORM;
use JsonSerializable;

/**
 * Сформированные предложения для пользователей
 *
 * @ORM\Table(
 *      indexes={
 *          @ORM\Index(name="idx__customer_id", columns={"customer_id"}),
 *          @ORM\Index(name="idx__preferences_rule_id", columns={"preferences_rule_id"}),
 *          @ORM\Index(name="idx__date", columns={"date"}),
 *          @ORM\Index(name="idx__is_deleted", columns={"is_deleted"}),
 *          @ORM\Index(name="idx__is_conversioned", columns={"is_conversioned"}),
 *          @ORM\Index(name="idx__is_liked", columns={"is_liked"}),
 *     }
 * )
 * @ORM\Entity(repositoryClass=PreferencesRepository::class)
 */
class Preferences implements JsonSerializable
{
    /**
     * @var int|null
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="bigint", options = {"unsigned":true, "comment":"Уникальный идентификатор записи"})
     */
    private $id;

    /**
     * @var int
     * @ORM\Column(type="bigint", options = {"unsigned":true, "comment":"Уникальный идентификатор заказчика для которого сделано предложение"})
     */
    private $customerId;

    /**
     * @var PreferencesRule
     * @ORM\ManyToOne(targetEntity=PreferencesRule::class)
     * @ORM\JoinColumn(nullable=false)
     */
    private $preferencesRule;

    /**
     * @var DateTime
     * @ORM\Column(type="date", options = {"comment":"На какую дату действует предложение"})
     */
    private $date;

    /**
     * @var int
     * @ORM\Column(type="bigint", options = {"unsigned":true, "comment":"Рекомендуемый продукт"})
     */
    private $tourproductId;

    /**
     * @var bool
     * @ORM\Column(type="boolean", options = {"comment":"Предложение удалено пользователем как не релевантное"})
     */
    private $isDeleted = false;

    /**
     * @var bool
     * @ORM\Column(type="boolean", options = {"comment":"Просмотрено"})
     */
    private $isConversioned = false;

    /**
     * @var boolean
     * @ORM\Column(type="boolean", options = {"comment":"Понравилось"})
     */
    private $isLiked = false;

    /**
     * @var boolean
     * @ORM\Column(type="boolean", options = {"comment":"Куплено"})
     */
    private $isPurchased = false;

    /**
     * @return int|null
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @return int
     */
    public function getCustomerId(): int
    {
        return $this->customerId;
    }

    /**
     * @param int $customerId
     * @return Preferences
     */
    public function setCustomerId(int $customerId): Preferences
    {
        $this->customerId = $customerId;
        return $this;
    }

    /**
     * @return PreferencesRule
     */
    public function getPreferencesRule(): PreferencesRule
    {
        return $this->preferencesRule;
    }

    /**
     * @param PreferencesRule $preferencesRule
     * @return Preferences
     */
    public function setPreferencesRule(PreferencesRule $preferencesRule): Preferences
    {
        $this->preferencesRule = $preferencesRule;
        return $this;
    }

    /**
     * @return DateTime
     */
    public function getDate(): DateTime
    {
        return $this->date;
    }

    /**
     * @param DateTime $date
     * @return Preferences
     */
    public function setDate(DateTime $date): Preferences
    {
        $this->date = $date;
        return $this;
    }

    /**
     * @return int
     */
    public function getTurproductId(): int
    {
        return $this->tourproductId;
    }

    /**
     * @param int $tourproductId
     * @return Preferences
     */
    public function setTourproductId(int $tourproductId): Preferences
    {
        $this->tourproductId = $tourproductId;
        return $this;
    }

    /**
     * @return bool
     */
    public function isDeleted(): bool
    {
        return $this->isDeleted;
    }

    /**
     * @param bool $isDeleted
     * @return Preferences
     */
    public function setIsDeleted(bool $isDeleted): Preferences
    {
        $this->isDeleted = $isDeleted;
        return $this;
    }

    /**
     * @return bool
     */
    public function isConversioned(): bool
    {
        return $this->isConversioned;
    }

    /**
     * @param bool $isConversioned
     * @return Preferences
     */
    public function setIsConversioned(bool $isConversioned): Preferences
    {
        $this->isConversioned = $isConversioned;
        return $this;
    }

    /**
     * @return bool
     */
    public function isLiked(): bool
    {
        return $this->isLiked;
    }

    /**
     * @param bool $isLiked
     * @return Preferences
     */
    public function setIsLiked(bool $isLiked): Preferences
    {
        $this->isLiked = $isLiked;
        return $this;
    }

    /**
     * @return bool
     */
    public function isPurchased(): bool
    {
        return $this->isPurchased;
    }

    /**
     * @param bool $isPurchased
     * @return Preferences
     */
    public function setIsPurchased(bool $isPurchased): Preferences
    {
        $this->isPurchased = $isPurchased;
        return $this;
    }
    
    /**
     * @inheritDoc
     */
    public function jsonSerialize(): array
    {
        return [
            "id"=>$this->getId(),
            "customer_id"=>(int)$this->customerId,
            "preferences_rule_id"=>$this->preferencesRule ? (int)$this->preferencesRule->getId() : null,
            "date"=>$this->date ? $this->date->format("Y-m-d"):null,
            "tourproduct_id"=>(int)$this->tourproductId,
            "is_deleted"=>$this->isDeleted,
            "is_conversioned"=>$this->isConversioned,
            "is_liked"=>$this->isLiked,
            "is_purchased"=>$this->isPurchased,
        ];
    }
}
