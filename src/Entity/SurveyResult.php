<?php

namespace App\Entity;

use App\Repository\SurveyResultRepository;
use DateTime;
use Doctrine\ORM\Mapping as ORM;
use JsonSerializable;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;


/**
 *  Результаты опросов
 *
 *  @ORM\Table(
 *      indexes={
 *          @ORM\Index(name="idx__survey_id", columns={"survey_id"}),
 *          @ORM\Index(name="idx__customer_id", columns={"customer_id"}),
 *          @ORM\Index(name="idx__survey_date_time", columns={"survey_date_time"}),
 *          @ORM\Index(name="idx__status", columns={"status"}),
 *          @ORM\Index(name="idx__processing_date_time", columns={"processing_date_time"}),
 *     }
 * )
 * @ORM\Entity(repositoryClass=SurveyResultRepository::class)
 */
class SurveyResult implements JsonSerializable
{
    /**
     * @var int|null
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="bigint", options = {"unsigned":true, "comment":"Уникальный идентификатор записи"})
     */
    private $id;

    /**
     * @var Survey
     * @ORM\ManyToOne(targetEntity=Survey::class)
     * @ORM\JoinColumn(nullable=false)
     */
    private $survey;

    /**
     * @var int
     * @ORM\Column(type="bigint", options = {"unsigned":true, "comment":"Уникальный идентификатор туриста"})
     */
    private $customerId;

    /**
     * @var DateTime
     * @ORM\Column(type="datetime", options = {"comment":"Дата и время проведения опроса"})
     */
    private $surveyDateTime;

    /**
     * @var int
     * @ORM\Column(type="smallint", options = {"unsigned":true,  "default": 0, "comment":"Статус"})
     *
     *  Возможные значения: 0 - не определен, 1 - в процессе заполнения, 2 - заполнен частично , 3 - заполнен
     */
    private $status = 0;

    /**
     * @var DateTime|null
     * @ORM\Column(type="datetime", nullable=true, options = {"unsigned":true, "comment":"Дата и время обработки результатов опроса"})
     */
    private $processingDateTime;

    /**
     * @var Collection|SurveyResultAnswers[]
     * @ORM\OneToMany(targetEntity=SurveyResultAnswers::class, mappedBy="surveyResult", fetch="EXTRA_LAZY")
     */
    private $surveyResultAnswers;


    public function __construct()
    {
        $this->surveyResultAnswers = new ArrayCollection();
    }

    /**
     * @return int|null
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @return Survey
     */
    public function getSurvey(): Survey
    {
        return $this->survey;
    }

    /**
     * @param Survey $survey
     * @return SurveyResult
     */
    public function setSurvey(Survey $survey): SurveyResult
    {
        $this->survey = $survey;
        return $this;
    }

    /**
     * @return int
     */
    public function getCustomerId(): int
    {
        return $this->customerId;
    }

    /**
     * @param int $customerId
     * @return SurveyResult
     */
    public function setCustomerId(int $customerId): SurveyResult
    {
        $this->customerId = $customerId;
        return $this;
    }

    /**
     * @return DateTime
     */
    public function getSurveyDateTime(): DateTime
    {
        return $this->surveyDateTime;
    }

    /**
     * @param DateTime $surveyDateTime
     * @return SurveyResult
     */
    public function setSurveyDateTime(DateTime $surveyDateTime): SurveyResult
    {
        $this->surveyDateTime = $surveyDateTime;
        return $this;
    }

    /**
     * @return int
     */
    public function getStatus(): int
    {
        return $this->status;
    }

    /**
     * @param int $status
     * @return SurveyResult
     */
    public function setStatus(int $status): SurveyResult
    {
        $this->status = $status;
        return $this;
    }

    /**
     * @return DateTime|null
     */
    public function getProcessingDateTime(): ?DateTime
    {
        return $this->processingDateTime;
    }

    /**
     * @param DateTime|null $processingDateTime
     * @return SurveyResult
     */
    public function setProcessingDateTime(?DateTime $processingDateTime): SurveyResult
    {
        $this->processingDateTime = $processingDateTime;
        return $this;
    }

    /**
     * @return Collection|SurveyResultAnswers[]
     */
    public function getSurveyResultAnswers()
    {
        return $this->surveyResultAnswers;
    }

    /**
     * @param SurveyResultAnswers[]|null $surveyResultAnswers
     * @return SurveyResult
     */
    public function setSurveyResultAnswers(?array $surveyResultAnswers): SurveyResult
    {
        $this->surveyResultAnswers = $surveyResultAnswers;
        return $this;
    }


    /**
     * @inheritDoc
     */
    public function jsonSerialize(): array
    {
        return [
            "id"=>$this->getId(),
            "survey_id"=>$this->survey ? $this->survey->getId() : null,
            "customer_id"=>(int)$this->customerId,
            "survey_date_time"=>$this->surveyDateTime ? $this->surveyDateTime->format("Y-m-d H:i:s"):null,
            "status"=>$this->status,
            "processing_date_time"=>$this->processingDateTime ? $this->processingDateTime->format("Y-m-d H:i:s"):null,
            "survey_result_answers"=>$this->surveyResultAnswers->toArray(),
        ];
    }


}
