<?php

namespace App\Entity;

use App\Repository\SurveyRepository;
use Doctrine\ORM\Mapping as ORM;
use egik\MicroserviceBundle\Entity\Common\AbstractLoggableEntity;
use JsonSerializable;
use Gedmo\Mapping\Annotation as Gedmo;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;


/**
 * Список опросов, для заполнения пользователями системы
 *
 *  @ORM\Table(
 *      indexes={
 *          @ORM\Index(name="idx__status", columns={"status"}),
 *          @ORM\Index(name="idx__is_required", columns={"is_required"}),
 *     }
 * )
 * @ORM\Entity(repositoryClass=SurveyRepository::class)
 */
class Survey extends AbstractLoggableEntity implements JsonSerializable
{
    /**
     * @var int|null
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="bigint", options = {"unsigned":true, "comment":"Уникальный идентификатор опроса"})
     */
    private $id;

    /**
     * @var string
     * @ORM\Column(type="string", length=128, options = { "comment":"Наименование опроса"})
     * @Gedmo\Versioned
     */
    private $title;

    /**
     * @var int
     * @ORM\Column(type="smallint", options = {"unsigned":true, "default": 0, "comment":"Статус опроса"})
     * @Gedmo\Versioned
     *
     * Возможные значения: 0 - черновик, 1 - модерация, 2 - отклонено, 3 - одобрено, 4 - опубликовано, 5 - снято с публикации, 6 - архив
     */
    private $status = 0;

    /**
     * @var int
     * @ORM\Column(type="smallint", options = {"unsigned":true, "default": 0, "comment":"Требовать обязательного заполнения"})
     * @Gedmo\Versioned
     */
    private $isRequired = 0;

    /**
     * @var int|null
     * @ORM\Column(type="integer", nullable=true, options = {"unsigned":true, "comment":"Повторять опрос(дней)"})
     * @Gedmo\Versioned
     */
    private $repeatDays;

    /**
     * @var Collection|SurveyQuestion[]
     * @ORM\OneToMany(targetEntity=SurveyQuestion::class, mappedBy="survey",  fetch="EXTRA_LAZY")
     */
    private $surveyQuestions;

    public function __construct()
    {
        $this->surveyQuestions = new ArrayCollection();

    }
    /**
     * @return int|null
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getTitle(): string
    {
        return $this->title;
    }

    /**
     * @param string $title
     * @return Survey
     */
    public function setTitle(string $title): Survey
    {
        $this->title = $title;
        return $this;
    }

    /**
     * @return int
     */
    public function getStatus(): int
    {
        return $this->status;
    }

    /**
     * @param int $status
     * @return Survey
     */
    public function setStatus(int $status): Survey
    {
        $this->status = $status;
        return $this;
    }

    /**
     * @return int
     */
    public function getIsRequired(): int
    {
        return $this->isRequired;
    }

    /**
     * @param int $isRequired
     * @return Survey
     */
    public function setIsRequired(int $isRequired): Survey
    {
        $this->isRequired = $isRequired;
        return $this;
    }

    /**
     * @return int|null
     */
    public function getRepeatDays(): ?int
    {
        return $this->repeatDays;
    }

    /**
     * @param int|null $repeatDays
     * @return Survey
     */
    public function setRepeatDays(?int $repeatDays): Survey
    {
        $this->repeatDays = $repeatDays;
        return $this;
    }

    /**
     * @return Collection|SurveyQuestion[]
     */
    public function getSurveyQuestions()
    {
        return $this->surveyQuestions;
    }

    /**
     * @param SurveyQuestion[]|null $surveyQuestions
     * @return Survey
     */
    public function setSurveyQuestions(?array $surveyQuestions): Survey
    {
        $this->surveyQuestions = $surveyQuestions;
        return $this;
    }

    /**
     * @inheritDoc
     */
    public function jsonSerialize(): array
    {
        return array_merge([
            "id"=>$this->getId(),
            "title"=>$this->title,
            "status"=>$this->status,
            "is_required"=>$this->isRequired,
            "repeat_days"=>$this->repeatDays,
            "survey_questions"=>$this->surveyQuestions->toArray(),
        ],parent::jsonSerialize());
    }

}
