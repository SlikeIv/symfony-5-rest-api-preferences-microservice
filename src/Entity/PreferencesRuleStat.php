<?php

namespace App\Entity;

use App\Repository\PreferencesRuleStatRepository;
use DateTime;
use Doctrine\ORM\Mapping as ORM;
use JsonSerializable;

/**
 * Статистика работы правила
 *
 * @ORM\Table(
 *      indexes={
 *          @ORM\Index(name="idx__preferences_rule_id", columns={"preferences_rule_id"}),
 *          @ORM\Index(name="idx__date", columns={"date"}),
 *     }
 * )
 * @ORM\Entity(repositoryClass=PreferencesRuleStatRepository::class)
 */
class PreferencesRuleStat  implements JsonSerializable
{
    /**
     * @var int|null
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="bigint", options = {"unsigned":true, "comment":"Уникальный идентификатор записи"})
     */
    private $id;

    /**
     * @var PreferencesRule
     * @ORM\ManyToOne(targetEntity=PreferencesRule::class)
     * @ORM\JoinColumn(nullable=false)
     */
    private $preferencesRule;

    /**
     * @var DateTime
     * @ORM\Column(type="date", options = {"comment":"За какую дату сформирована запись"})
     */
    private $date;

    /**
     * @var int
     * @ORM\Column(type="integer", options = {"unsigned":true, "default": 0, "comment":"Подготовлено рекомендаций на основе правила"})
     */
    private $generatedPreferences = 0;

    /**
     * @var int
     * @ORM\Column(type="integer", options = {"unsigned":true, "default": 0, "comment":"Удалено рекомендаций на основе правила"})
     */
    private $deleted = 0;

    /**
     * @var int
     * @ORM\Column(type="integer", options = {"unsigned":true, "default": 0, "comment":"Переходов по рекомендации"})
     */
    private $conversion = 0;

    /**
     * @var int
     * @ORM\Column(type="integer", options = {"unsigned":true, "default": 0, "comment":"Покупок по рекомендации"})
     */
    private $purchases = 0;

    /**
     * @var float
     * @ORM\Column(type="decimal", precision=10, scale=2, options = {"default": 0, "unsigned":true, "comment":"Уникальный идентификатор записи"})
     */
    private $purchasesSum = 0;

    /**
     * @var int
     * @ORM\Column(type="integer", options = {"unsigned":true, "default": 0, "comment":"Сумма покупок"})
     */
    private $likeCount = 0;

    /**
     * @var int
     * @ORM\Column(type="integer", options = {"unsigned":true, "default": 0, "comment":"Количество удалений предложений по рекомендаций"})
     */
    private $deleteCount = 0;

    public function __construct()
    {
        $this->date = new DateTime();
    }

    /**
     * @return int|null
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @return PreferencesRule
     */
    public function getPreferencesRule(): PreferencesRule
    {
        return $this->preferencesRule;
    }

    /**
     * @param PreferencesRule $preferencesRule
     * @return PreferencesRuleStat
     */
    public function setPreferencesRule(PreferencesRule $preferencesRule): PreferencesRuleStat
    {
        $this->preferencesRule = $preferencesRule;
        return $this;
    }

    /**
     * @return DateTime
     */
    public function getDate(): DateTime
    {
        return $this->date;
    }

    /**
     * @param DateTime $date
     * @return PreferencesRuleStat
     */
    public function setDate(DateTime $date): PreferencesRuleStat
    {
        $this->date = $date;
        return $this;
    }

    /**
     * @return int
     */
    public function getGeneratedPreferences(): int
    {
        return $this->generatedPreferences;
    }

    /**
     * @param int $generatedPreferences
     * @return PreferencesRuleStat
     */
    public function setGeneratedPreferences(int $generatedPreferences): PreferencesRuleStat
    {
        $this->generatedPreferences = $generatedPreferences;
        return $this;
    }

    /**
     * @return int
     */
    public function getDeleted(): int
    {
        return $this->deleted;
    }

    /**
     * @param int $deleted
     * @return PreferencesRuleStat
     */
    public function setDeleted(int $deleted): PreferencesRuleStat
    {
        $this->deleted = $deleted;
        return $this;
    }

    /**
     * @return int
     */
    public function getConversion(): int
    {
        return $this->conversion;
    }

    /**
     * @param int $conversion
     * @return PreferencesRuleStat
     */
    public function setConversion(int $conversion): PreferencesRuleStat
    {
        $this->conversion = $conversion;
        return $this;
    }

    /**
     * @return int
     */
    public function getPurchases(): int
    {
        return $this->purchases;
    }

    /**
     * @param int $purchases
     * @return PreferencesRuleStat
     */
    public function setPurchases(int $purchases): PreferencesRuleStat
    {
        $this->purchases = $purchases;
        return $this;
    }

    /**
     * @return float
     */
    public function getPurchasesSum(): float
    {
        return $this->purchasesSum;
    }

    /**
     * @param float $purchasesSum
     * @return PreferencesRuleStat
     */
    public function setPurchasesSum(float $purchasesSum): PreferencesRuleStat
    {
        $this->purchasesSum = $purchasesSum;
        return $this;
    }

    /**
     * @return int
     */
    public function getLikeCount(): int
    {
        return $this->likeCount;
    }

    /**
     * @param int $likeCount
     * @return PreferencesRuleStat
     */
    public function setLikeCount(int $likeCount): PreferencesRuleStat
    {
        $this->likeCount = $likeCount;
        return $this;
    }

    /**
     * @return int
     */
    public function getDeleteCount(): int
    {
        return $this->deleteCount;
    }

    /**
     * @param int $deleteCount
     * @return PreferencesRuleStat
     */
    public function setDeleteCount(int $deleteCount): PreferencesRuleStat
    {
        $this->deleteCount = $deleteCount;
        return $this;
    }

    /**
     * @inheritDoc
     */
    public function jsonSerialize(): array
    {
        return [
            "id"=>$this->getId(),
            "preferences_rule_id"=>$this->preferencesRule,
            "date"=>$this->date ? $this->date->format("Y-m-d"):null,
            "generated_preferences"=>$this->generatedPreferences,
            "deleted"=>$this->deleted,
            "conversion"=>$this->conversion,
            "purchases"=>$this->purchases,
            "purchases_sum"=>$this->purchasesSum,
            "like_count"=>$this->likeCount,
            "delete_count"=>$this->deleteCount
        ];
    }


}
