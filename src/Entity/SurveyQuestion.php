<?php

namespace App\Entity;

use App\Repository\SurveyQuestionRepository;
use Doctrine\ORM\Mapping as ORM;
use egik\MicroserviceBundle\Entity\Common\AbstractLoggableEntity;
use JsonSerializable;
use Gedmo\Mapping\Annotation as Gedmo;


/**
 * Вопросы входящие в опрос
 *
 *  @ORM\Table(
 *      indexes={
 *          @ORM\Index(name="idx__survey_id", columns={"survey_id"}),
 *          @ORM\Index(name="idx__position", columns={"position"}),
 *     }
 * )
 * @ORM\Entity(repositoryClass=SurveyQuestionRepository::class)
 */
class SurveyQuestion extends AbstractLoggableEntity implements JsonSerializable
{
    /**
     * @var int|null
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="bigint", options = {"unsigned":true, "comment":"Уникальный идентификатор записи"})
     */
    private $id;

    /**
     * @var Survey
     * @ORM\ManyToOne(targetEntity=Survey::class, inversedBy="surveyQuestions")
     * @ORM\JoinColumn(nullable=false)
     * @Gedmo\Versioned
     */
    private $survey;

    /**
     * @var string
     * @ORM\Column(type="string", length=128, options = {"comment":"Текст вопроса"})
     * @Gedmo\Versioned
     */
    private $text;

    /**
     * @var string|null
     * @ORM\Column(type="string", length=512, nullable=true, options = {"comment":"Расширенное описание"})
     * @Gedmo\Versioned
     */
    private $description;

    /**
     * @var int
     * @ORM\Column(type="smallint", options = {"unsigned":true, "default": 0, "comment":"Тип вопроса"})
     * @Gedmo\Versioned
     *
     * Возможные значения: 0 - число, 1 - строка , 2 - выпадающий список, 3 - список с выбором нескольких значений, 4 - радио список, 5 - дата, 6 - диапазон дат
     */
    private $type = 0;

    /**
     * @var int
     * @ORM\Column(type="smallint", options = {"unsigned":true, "default": 1, "comment":"Можно пропустить"})
     * @Gedmo\Versioned
     */
    private $canSkip = 1;

    /**
     * @var array
     * @ORM\Column(type="json", options = {"comment":"Дополнительные параметры вопроса"})
     * @Gedmo\Versioned
     */
    private $params = [];

    /**
     * @var int
     * @ORM\Column(type="integer", options = {"unsigned":true, "comment":"Номер вопроса в опросе"})
     * @Gedmo\Versioned
     */
    private $position;

    /**
     * @return int|null
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @return Survey
     */
    public function getSurvey(): Survey
    {
        return $this->survey;
    }

    /**
     * @param Survey $survey
     * @return SurveyQuestion
     */
    public function setSurvey(Survey $survey): SurveyQuestion
    {
        $this->survey = $survey;
        return $this;
    }

    /**
     * @return string
     */
    public function getText(): string
    {
        return $this->text;
    }

    /**
     * @param string $text
     * @return SurveyQuestion
     */
    public function setText(string $text): SurveyQuestion
    {
        $this->text = $text;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getDescription(): ?string
    {
        return $this->description;
    }

    /**
     * @param string|null $description
     * @return SurveyQuestion
     */
    public function setDescription(?string $description): SurveyQuestion
    {
        $this->description = $description;
        return $this;
    }

    /**
     * @return int
     */
    public function getType(): int
    {
        return $this->type;
    }

    /**
     * @param int $type
     * @return SurveyQuestion
     */
    public function setType(int $type): SurveyQuestion
    {
        $this->type = $type;
        return $this;
    }

    /**
     * @return int
     */
    public function getCanSkip(): int
    {
        return $this->canSkip;
    }

    /**
     * @param int $canSkip
     * @return SurveyQuestion
     */
    public function setCanSkip(int $canSkip): SurveyQuestion
    {
        $this->canSkip = $canSkip;
        return $this;
    }

    /**
     * @return array
     */
    public function getParams(): array
    {
        return $this->params;
    }

    /**
     * @param array $params
     * @return SurveyQuestion
     */
    public function setParams(array $params): SurveyQuestion
    {
        $this->params = $params;
        return $this;
    }

    /**
     * @return int
     */
    public function getPosition(): int
    {
        return $this->position;
    }

    /**
     * @param int $position
     * @return SurveyQuestion
     */
    public function setPosition(int $position): SurveyQuestion
    {
        $this->position = $position;
        return $this;
    }

    /**
     * @inheritDoc
     */
    public function jsonSerialize(): array
    {
        return array_merge([
            "id"=>$this->getId(),
            "survey_id"=>$this->survey ? $this->survey->getId() : null,
            "text"=>$this->text,
            "description"=>$this->description,
            "type"=>$this->type,
            "can_skip"=>$this->canSkip,
            "params"=>$this->params,
            "position"=>$this->position,
        ],parent::jsonSerialize());
    }


}
