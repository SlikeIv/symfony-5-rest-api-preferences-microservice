<?php

namespace App\Entity;

use App\Repository\SurveyResultAnswersRepository;
use Doctrine\ORM\Mapping as ORM;
use JsonSerializable;

/**
 *  Результаты ответов на опрос
 *
 *  @ORM\Table(
 *      indexes={
 *          @ORM\Index(name="idx__survey_result_id", columns={"survey_result_id"}),
 *          @ORM\Index(name="idx__question_id", columns={"question_id"}),
 *     }
 * )
 * @ORM\Entity(repositoryClass=SurveyResultAnswersRepository::class)
 */
class SurveyResultAnswers  implements JsonSerializable
{
    /**
     * @var int|null
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="bigint", options = {"unsigned":true, "comment":"Уникальный идентификатор записи"})
     */
    private $id;

    /**
     * @var SurveyResult
     * @ORM\ManyToOne(targetEntity=SurveyResult::class, inversedBy="surveyResultAnswers")
     * @ORM\JoinColumn(nullable=false)
     */
    private $surveyResult;

    /**
     * @var SurveyQuestion
     * @ORM\ManyToOne(targetEntity=SurveyQuestion::class)
     * @ORM\JoinColumn(nullable=false)
     */
    private $question;

    /**
     * @var array
     * @ORM\Column(type="json", options = {"comment":"Ответ на вопрос"})
     */
    private $answer = [];

    /**
     * @return int|null
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @return SurveyResult
     */
    public function getSurveyResult(): SurveyResult
    {
        return $this->surveyResult;
    }

    /**
     * @param SurveyResult $surveyResult
     * @return SurveyResultAnswers
     */
    public function setSurveyResult(SurveyResult $surveyResult): SurveyResultAnswers
    {
        $this->surveyResult = $surveyResult;
        return $this;
    }

    /**
     * @return SurveyQuestion
     */
    public function getQuestion(): SurveyQuestion
    {
        return $this->question;
    }

    /**
     * @param SurveyQuestion $question
     * @return SurveyResultAnswers
     */
    public function setQuestion(SurveyQuestion $question): SurveyResultAnswers
    {
        $this->question = $question;
        return $this;
    }

    /**
     * @return array
     */
    public function getAnswer(): array
    {
        return $this->answer;
    }

    /**
     * @param array $answer
     * @return SurveyResultAnswers
     */
    public function setAnswer(array $answer): SurveyResultAnswers
    {
        $this->answer = $answer;
        return $this;
    }

    /**
     * @inheritDoc
     */
    public function jsonSerialize(): array
    {
        return [
            "id"=>$this->getId(),
            "survey_result_id"=>$this->surveyResult ? $this->surveyResult->getId() : null,
            "question_id"=>$this->question ? $this->question->getId() : null,
            "answer"=>$this->answer,
        ];
    }




}
