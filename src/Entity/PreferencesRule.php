<?php

namespace App\Entity;

use App\Repository\PreferencesRuleRepository;
use DateTime;
use Doctrine\ORM\Mapping as ORM;
use JsonSerializable;
use egik\MicroserviceBundle\Entity\Common\AbstractLoggableEntity;
use Gedmo\Mapping\Annotation as Gedmo;

/**
 * Статистика работы правила
 *
 * @ORM\Table(
 *      indexes={
 *          @ORM\Index(name="idx__status", columns={"status"}),
 *          @ORM\Index(name="idx__type", columns={"type"}),
 *          @ORM\Index(name="idx__date_start", columns={"date_start"}),
 *          @ORM\Index(name="idx__date_end", columns={"date_end"}),
 *          @ORM\Index(name="idx__is_active", columns={"is_active"}),
 *     }
 * )
 * @ORM\Entity(repositoryClass=PreferencesRuleRepository::class)
 */

class PreferencesRule extends AbstractLoggableEntity implements JsonSerializable
{
    /**
     * @var int|null
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="bigint", options = {"unsigned":true, "comment":"Уникальный идентификатор правила"})
     */
    private $id;

    /**
     * @var int
     * @ORM\Column(type="smallint", options = {"unsigned":true, "default": 0, "comment":"Статус правила"})
     * @Gedmo\Versioned
     *
     * Возможные значения: 0 - черновик, 1 - модерация, 2 - отклонено, 3 - одобрено, 4 - опубликовано, 5 - снято с публикации, 6 - архив
     */
    private $status = 0;

    /**
     * @var int
     * @ORM\Column(type="smallint", options = {"unsigned":true, "comment":"Тип правила "})
     * @Gedmo\Versioned
     *
     * Возможные значения:
     *  0 - не определено
     *  1 - на основе собственных лайков
     *  2 - на основе собственных оценок рейтинга
     *  3 - на основе общего рейтинга
     *  4 - на основе опроса
     *  5 - на основе предыдущих покупок
     *  6 - на основе предыдущих просмотров
     *  7 - исключать из выбора негативные оценки(дизлайки)
     *  8 - исключать из выбора возвраты покупок(персональные)
     *  9 - исключать из выбора возвраты покупок(общие)
     */
    private $type;

    /**
     * @var int
     * @ORM\Column(type="smallint", options = {"unsigned":true, "default": 3,  "comment":"Учитывать не менее N(лайков, покупок, просмотров)"})
     * @Gedmo\Versioned
     */
    private $minCount = 3;

    /**
     * @var int
     * @ORM\Column(type="smallint", options = {"unsigned":true, "default": 5, "comment":"Учитывать предложения только с рейтингом не ниже"})
     * @Gedmo\Versioned
     */
    private $minRating = 5;

    /**
     * @var int|null
     * @ORM\Column(type="smallint", nullable=true, options = {"unsigned":true, "comment":"Ограничить тип продукта"})
     * @Gedmo\Versioned
     */
    private $productType;

    /**
     * @var int
     * @ORM\Column(type="smallint", options = {"unsigned":true, "default": 3, "comment":"Ограничение выдачи рекомендаций, по правилу(не больше)"})
     * @Gedmo\Versioned
     */
    private $showCount = 3;

    /**
     * @var int
     * @ORM\Column(type="smallint", options = {"unsigned":true, "comment":"Тип выдачи"})
     * @Gedmo\Versioned
     *
     * Возможные значения: 0 - не определено, 1 - с хорошим рейтингом сначала, 2 - случайно, 3 - сначала новые
     */
    private $showType;

    /**
     * @var DateTime
     * @ORM\Column(type="date", options = { "comment":"Начало действия правила"})
     * @Gedmo\Versioned
     */
    private $dateStart;

    /**
     * @var DateTime|null
     * @ORM\Column(type="date", nullable=true, options = {"comment":"Окончание действия правила"})
     * @Gedmo\Versioned
     */
    private $dateEnd;

    /**
     * @var int
     * @ORM\Column(type="smallint", options = {"unsigned":true, "comment":"Правило активно"})
     * @Gedmo\Versioned
     */
    private $isActive;

    /**
     * @var int
     * @ORM\Column(type="smallint", options = {"unsigned":true, "comment":"Применять правило к сформированному предложению "})
     * @Gedmo\Versioned
     */
    private $isFilter;

    /**
     * @var int
     * @ORM\Column(type="smallint", options = {"unsigned":true, "comment":"Останавливать подбор после срабатывания правила"})
     * @Gedmo\Versioned
     */
    private $isStop;

    public function __construct()
    {
        $this->dateStart = new DateTime();
    }

    /**
     * @return int|null
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @return int
     */
    public function getStatus(): int
    {
        return $this->status;
    }

    /**
     * @param int $status
     * @return PreferencesRule
     */
    public function setStatus(int $status): PreferencesRule
    {
        $this->status = $status;
        return $this;
    }

    /**
     * @return int
     */
    public function getType(): int
    {
        return $this->type;
    }

    /**
     * @param int $type
     * @return PreferencesRule
     */
    public function setType(int $type): PreferencesRule
    {
        $this->type = $type;
        return $this;
    }

    /**
     * @return int
     */
    public function getMinCount(): int
    {
        return $this->minCount;
    }

    /**
     * @param int $minCount
     * @return PreferencesRule
     */
    public function setMinCount(int $minCount): PreferencesRule
    {
        $this->minCount = $minCount;
        return $this;
    }

    /**
     * @return int
     */
    public function getMinRating(): int
    {
        return $this->minRating;
    }

    /**
     * @param int $minRating
     * @return PreferencesRule
     */
    public function setMinRating(int $minRating): PreferencesRule
    {
        $this->minRating = $minRating;
        return $this;
    }

    /**
     * @return int|null
     */
    public function getProductType(): ?int
    {
        return $this->productType;
    }

    /**
     * @param int|null $productType
     * @return PreferencesRule
     */
    public function setProductType(?int $productType): PreferencesRule
    {
        $this->productType = $productType;
        return $this;
    }

    /**
     * @return int
     */
    public function getShowCount(): int
    {
        return $this->showCount;
    }

    /**
     * @param int $showCount
     * @return PreferencesRule
     */
    public function setShowCount(int $showCount): PreferencesRule
    {
        $this->showCount = $showCount;
        return $this;
    }

    /**
     * @return int
     */
    public function getShowType(): int
    {
        return $this->showType;
    }

    /**
     * @param int $showType
     * @return PreferencesRule
     */
    public function setShowType(int $showType): PreferencesRule
    {
        $this->showType = $showType;
        return $this;
    }

    /**
     * @return DateTime
     */
    public function getDateStart(): DateTime
    {
        return $this->dateStart;
    }

    /**
     * @param DateTime $dateStart
     * @return PreferencesRule
     */
    public function setDateStart(DateTime $dateStart): PreferencesRule
    {
        $this->dateStart = $dateStart;
        return $this;
    }

    /**
     * @return DateTime|null
     */
    public function getDateEnd(): ?DateTime
    {
        return $this->dateEnd;
    }

    /**
     * @param DateTime|null $dateEnd
     * @return PreferencesRule
     */
    public function setDateEnd(?DateTime $dateEnd): PreferencesRule
    {
        $this->dateEnd = $dateEnd;
        return $this;
    }

    /**
     * @return int
     */
    public function getIsActive(): int
    {
        return $this->isActive;
    }

    /**
     * @param int $isActive
     * @return PreferencesRule
     */
    public function setIsActive(int $isActive): PreferencesRule
    {
        $this->isActive = $isActive;
        return $this;
    }

    /**
     * @return int
     */
    public function getIsFilter(): int
    {
        return $this->isFilter;
    }

    /**
     * @param int $isFilter
     * @return PreferencesRule
     */
    public function setIsFilter(int $isFilter): PreferencesRule
    {
        $this->isFilter = $isFilter;
        return $this;
    }

    /**
     * @return int
     */
    public function getIsStop(): int
    {
        return $this->isStop;
    }

    /**
     * @param int $isStop
     * @return PreferencesRule
     */
    public function setIsStop(int $isStop): PreferencesRule
    {
        $this->isStop = $isStop;
        return $this;
    }


    /**
     * @inheritDoc
     */
    public function jsonSerialize(): array
    {
        return array_merge([
          "id"=>$this->getId(),
          "status"=>$this->status,
          "type"=>$this->type,
          "min_count"=>$this->minCount,
          "min_rating"=>$this->minRating,
          "product_type"=>$this->productType,
          "show_count"=>$this->showCount,
          "show_type"=>$this->showType,
          "date_start"=>$this->dateStart ? $this->dateStart->format("Y-m-d"):null,
          "date_end"=>$this->dateEnd ? $this->dateEnd->format("Y-m-d"):null,
          "is_active"=>$this->isActive,
          "is_filter"=>$this->isFilter,
          "is_stop"=>$this->isStop,
        ],parent::jsonSerialize());
    }

}
