<?php
namespace App\Service;

use App\Entity\SurveyQuestion;
use DateTime;
use Doctrine\Common\Collections\Collection;
use egik\MicroserviceApi\Tourproduct\Tourproduct\Request\ListRequest as TourproductListRequest;
use egik\MicroserviceApi\Object\InfraObject\Request\ListRequest as ObjectListRequest;
use App\Entity\SurveyResult;
use App\Repository\SurveyResultRepository;
use App\Entity\PreferencesRule;
use App\Repository\PreferencesRuleRepository;
use App\Entity\Preferences;
use App\Repository\PreferencesRepository;
use egik\MicroserviceApi\LikeRating\Rating\Request\StatRequest as LikeRatingStatRequest;
use egik\MicroserviceApi\Object\Client as ObjectClient;
use egik\MicroserviceApi\Tourproduct\Client as TourproductClient;
use egik\MicroserviceApi\LikeRating\Client as LikeRatingClient;
use egik\MicroserviceApi\Base\Reference\Request\ListRequest as ReferenceListRequest;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\Common\Collections\Criteria;

class PreferencesService
{

    /**
     * @var TourproductClient
     */
    private $tourproductClient;

    /**
     * @var ObjectClient
     */
    private $objectClient;

    /**
     * @var LikeRatingClient
     */
    private $likeRatingClient;

    /**
     * @var EntityManagerInterface
     */
    private $entityManager;


    /**
     * @param TourproductClient $tourproductClient
     * @param ObjectClient $objectClient
     * @param LikeRatingClient $likeRatingClient
     * @param EntityManagerInterface $entityManager
     */
    public function __construct(TourproductClient $tourproductClient, ObjectClient $objectClient, LikeRatingClient $likeRatingClient, EntityManagerInterface $entityManager)
    {

        $this->tourproductClient=$tourproductClient;
        $this->objectClient=$objectClient;
        $this->entityManager=$entityManager;
        $this->likeRatingClient=$likeRatingClient;
    }


    public function generatePreferences()
    {
        // get all tourproducts
        $allTourProducts = $this->geAllTourproducts();

        /** @var SurveyResultRepository $surveyResultsRepository */
        $surveyResultsRepository=$this->entityManager->getRepository(SurveyResult::class);
        // get all finished survey results
        $surveyResults = $surveyResultsRepository->findBy(['status' => 3]);

        /** @var PreferencesRuleRepository $preferencesRuleRepository */
        $preferencesRuleRepository=$this->entityManager->getRepository(PreferencesRule::class);
        // get last & active rule
        $preferencesRule = $this->getActivePreferencesRule();
        //check exist rule
        if(is_null($preferencesRule)) {
            echo "no active rule \r\n";
            return;
        }
        $countShowPreferences = $preferencesRule->getShowCount();

        if(!empty($surveyResults)) {
            foreach ($surveyResults as $result) {
                $relevantTourProductIds = [];  // массив где сохраняются id тупродукта в качестве ключа массива, значениями будет приоритет и рейтинг
                $surveyAnswers = $result->getSurveyResultAnswers();
                $survey = $result->getSurvey();
                $customerId = $result->getCustomerId();
                $questions = $survey->getSurveyQuestions();
                $assocQuestionIdAndProcessingType = $this->getAssocQuestionIdAndProcessingType($questions);


                // фильтруем турпродукты по ответам из опроса
                foreach ($surveyAnswers as $item) {
                    $questionId = $item->getQuestion()->getId();
                    $answer = $item->getAnswer();
                    $processingType = $assocQuestionIdAndProcessingType[$questionId];
                    switch ($processingType) {
                        case 'tourproducttype':
                            $this->findRelevantProductsByTourProductType($relevantTourProductIds, $allTourProducts, $answer);
                            break;
                        case "objecttype":
                            $this->findRelevantProductsByObjectType($relevantTourProductIds,$allTourProducts, $answer);
                            break;
                        case 'servicetype':
                            $this->findRelevantProductsByServiceType($relevantTourProductIds,$allTourProducts, $answer);
                            break;
                        case "movementmethod":
                            $this->findRelevantProductsByMovementMethod($relevantTourProductIds,$allTourProducts, $answer);
                            break;
                        case "remoteness":
                            $this->findRelevantProductsByRemoteness($relevantTourProductIds, $allTourProducts, $answer);
                            break;
                        default:
                            //nothing
                    }
                }

                // если после фильтрации по ответам и рейтингу количество тупродуктов в выборке меньше чем указано в 'show_count', добавляем недостающее количество из заранее отсортированного $allTourProducts
                if(count($relevantTourProductIds) < $countShowPreferences) {
                    $neededAddProducts =  $countShowPreferences - count($relevantTourProductIds);
                    foreach ($allTourProducts as $tourProduct) {
                        if(!array_key_exists($tourProduct['id'], $relevantTourProductIds) & $neededAddProducts > 0) {
                            $relevantTourProductIds[$tourProduct['id']] = [
                                'edit_date_time' => $tourProduct['edit_date_time'],
                                "rating" => $tourProduct['rating'],
                                "priority" => 0,
                            ];
                            $neededAddProducts--;
                        }
                    }
                }else{
                    // обрезаем выборку по параметру 'show_count' правила если количество превышает 'show_count'
                    $relevantTourProductIds = array_slice($relevantTourProductIds, 0, $countShowPreferences, true);
                }

                // сортируем выборку для каждого пользователя
                $this->sortRelevantProducts($relevantTourProductIds);

                // save relevant tourproducts in preference table
                if(count($relevantTourProductIds) > 0) {
                    foreach ($relevantTourProductIds as $tourProductId => $relevantData) {
                        $preference = new Preferences();
                        $preference->setCustomerId($result->getCustomerId());
                        $preference->setTourproductId($tourProductId);
                        $preference->setPreferencesRule($preferencesRule);
                        $preference->setDate(new DateTime());
                        $this->entityManager->persist($preference);
                    }
                    $this->entityManager->flush();
                }
            }
        }

    }

    /**
     * @return PreferencesRule|null
     */
    private function getActivePreferencesRule(): ?PreferencesRule {
        /** @var PreferencesRuleRepository $preferencesRuleRepository */
        $preferencesRuleRepository=$this->entityManager->getRepository(PreferencesRule::class);
        // get last & active rule
        $expr = Criteria::expr();
        $criteria = Criteria::create();
        $criteria->where($expr->lte('dateStart', new DateTime('now')));
        $criteria->andWhere($expr->gte('dateEnd', new DateTime('now')));
        $criteria->andWhere($expr->eq('status', 4));
        $criteria->orderBy(["id" => 'desc']);
        $criteria->setMaxResults(1);
        $preferencesRules = $preferencesRuleRepository->matching($criteria);
        if(!empty($preferencesRules)) {
            return $preferencesRules[0];
        }
        return null;
    }

    /**
     * @param array $relevantTourProductIds
     */
    private function sortRelevantProducts(array &$relevantTourProductIds): void {
        // заполняем нулями отсутствующие поля
        foreach ($relevantTourProductIds as $key => $tourProductId) {
            if(empty($tourProductId['rating'])) $relevantTourProductIds[$key]['rating'] = 0;
            if(empty($tourProductId['priority'])) $relevantTourProductIds[$key]['priority'] = 0;
        }
        // сортировка по приоритету, потом по рейтингу
        uasort($relevantTourProductIds, function($a, $b){
            //сортировка по priority
            if ($a['priority'] == $b['priority']) {
                //сортировка по rating
                if ($a['rating'] < $b['rating']) {
                    return 1;
                }elseif($a['rating'] > $b['rating']) {
                    return -1;
                }else{
                     //сортировка по edit_date_time
                    if ($a['edit_date_time'] < $b['edit_date_time']) {
                        return 1;
                    }elseif($a['edit_date_time'] > $b['edit_date_time']) {
                        return -1;
                    }else {
                        return 0;
                    }
                }
            }
            return ($a['priority'] < $b['priority']) ? 1 : -1;
        });

    }


    /**
     * @param float $latitude
     * @param float $longitude
     * @return bool
     */
    private function inKalinigradCityArea(float $latitude, float $longitude): bool {
        // Kalinigrad border point
        $leftBottomPoint = ["latitude"=> 54.642964, "longitude" => 20.324350];
        $rightTopPoint = ["latitude"=> 54.781177, "longitude" => 20.649342];

        if( $latitude < $leftBottomPoint["latitude"] || $latitude > $rightTopPoint["latitude"] ) {
            return false;
        }
        if( $longitude < $leftBottomPoint["longitude"] ||  $longitude > $rightTopPoint["longitude"] ) {
            return false;
        }
        return true;
    }


    /**
     * @param array $relevantTourProductsIds
     * @param array $tourProduct
     * @param int $addPriority
     */
    private function updatePriority(array &$relevantTourProductsIds, array $tourProduct, int $addPriority=1) :void {
        $tourProductId = $tourProduct['id'];
        if(empty($relevantTourProductsIds[$tourProductId])) {
            $relevantTourProductsIds[$tourProductId]= [];
            $relevantTourProductsIds[$tourProductId]['priority'] = $addPriority;
        }else{
            $relevantTourProductsIds[$tourProductId]['priority'] =  $relevantTourProductsIds[$tourProductId]['priority'] + $addPriority;
        }
        $relevantTourProductsIds[$tourProductId]['edit_date_time'] = $tourProduct['edit_date_time'];
        $relevantTourProductsIds[$tourProductId]['rating'] = $tourProduct['rating'];
    }


    /**
     * @param array $relevantTourProductsIds
     * @param array $allTourProducts
     * @param array $answer
     */
    private function findRelevantProductsByTourProductType(array &$relevantTourProductsIds, array $allTourProducts, array $answer): void {
        // if answer not empty
        if(is_array($answer) & count($answer) > 0) {
            foreach ($allTourProducts as $tourProduct) {
                // check exist tourproduct type exist in answer
                $type = $tourProduct['type'];
                if(in_array($type, $answer)) {
                    $this->updatePriority($relevantTourProductsIds, $tourProduct, 1);
                }

            }
        }
    }

    /**
     * @param array $relevantTourProductsIds
     * @param array $allTourProducts
     * @param array $answer
     */
    private function findRelevantProductsByServiceType(array &$relevantTourProductsIds, array $allTourProducts, array $answer): void {
        // if answer not empty
        if(is_array($answer) & count($answer) > 0) {
            foreach ($allTourProducts as $tourProduct) {
                if(!empty($tourProduct['service_type'])) {
                    $serviceType = $tourProduct['service_type'];
                    if(in_array($serviceType, $answer)) {
                        $this->updatePriority($relevantTourProductsIds, $tourProduct, 1);
                    }
                }
            }
        }
    }


    /**
     * @param array $relevantTourProductsIds
     * @param array $allTourProducts
     * @param array $answer
     */
    private function findRelevantProductsByObjectType(array &$relevantTourProductsIds, array $allTourProducts, array $answer): void {
        // if answer not empty
        if(is_array($answer) & count($answer) > 0) {
            foreach ($allTourProducts as $tourProduct) {
                if(!empty($tourProduct['object_types']) ) {
                    $objectTypes = $tourProduct['object_types'];
                    $objectExist = false;
                    foreach ($objectTypes as $objectType) {
                        if(in_array($objectType, $answer)) {
                            $objectExist = true;
                            break;
                        }
                    }
                    if ($objectExist === true) {
                        $this->updatePriority($relevantTourProductsIds, $tourProduct, 1);
                    }
                }
            }
        }
    }


    /**
     * @param array $relevantTourProductsIds
     * @param array $allTourProducts
     * @param array $answer
     */
    private function findRelevantProductsByMovementMethod(array &$relevantTourProductsIds, array $allTourProducts, array $answer): void {
        // if answer not empty
        if(is_array($answer) & count($answer) > 0) {
            foreach ($allTourProducts as $tourProduct) {
                if(!empty($tourProduct['movement_type'])) {
                    $movementType = $tourProduct['movement_type'];
                    if(in_array($movementType, $answer)) {
                        $this->updatePriority($relevantTourProductsIds, $tourProduct, 1);
                    }
                }
            }
        }
    }


    /**
     * @param array $relevantTourProductsIds
     * @param array $allTourProducts
     * @param array $answer
     */
    private function findRelevantProductsByRemoteness(array &$relevantTourProductsIds, array $allTourProducts, array $answer): void {
        // if answer not empty
        if(is_array($answer) & count($answer) > 0) {
            foreach ($allTourProducts as $tourProduct) {
                $lat = $tourProduct['lat'];
                $lon = $tourProduct['lon'];
                $inKaliningrad = false;
                if(!empty($lat) & !empty($lon)) {
                    if($this->inKalinigradCityArea($lat, $lon)) {
                        $inKaliningrad = true;
                    }
                    if($answer[0] === 'inKaliningrad' & $inKaliningrad === true) {
                        $this->updatePriority($relevantTourProductsIds, $tourProduct, 1);
                    }elseif ($answer[0] === 'outsideKaliningrad' & $inKaliningrad === false) {
                        $this->updatePriority($relevantTourProductsIds, $tourProduct, 1);
                    }

                }
            }
        }
    }

    /**
     * @param Collection|SurveyQuestion[] $questions
     * @return array
     */
    private function getAssocQuestionIdAndProcessingType($questions): array {
        $assocQuestionIdAndProcessingType = [];
        foreach ($questions as $key => $item) {
            $assocQuestionIdAndProcessingType[$item->getId()] =  $item->getParams()['processing_type'];
        }
        return $assocQuestionIdAndProcessingType;
    }

    /**
     * @return array
     */
    private function geAllTourproducts ():array {

        $request = new TourproductListRequest();
        $tourproductListResponse=$this->tourproductClient->tourproduct()->list($request);
        $allTourProductsResponse = $tourproductListResponse->getList();

        $referenceListRequest=new ReferenceListRequest();
        $referenceListRequest->setType("tourproducttype");
        $allTourProductTypesResponse = $this->tourproductClient->reference()->list($referenceListRequest);
        $tourproductTypesByValue = [];
        foreach ( $allTourProductTypesResponse->getItems() as $tourproductType){
            $tourproductTypesByValue[$tourproductType->getValue()] = $tourproductType->getName();
        }

        $referenceListRequest->setType("servicetype");
        $allServiceTypesResponse = $this->tourproductClient->reference()->list($referenceListRequest);
        $serviceTypesByValue = [];
        foreach ( $allServiceTypesResponse->getItems() as $serviceType){
            $serviceTypesByValue[$serviceType->getValue()] = $serviceType->getName();
        }

        $referenceListRequest->setType("movementmethod");
        $allMovementMethodsResponse = $this->tourproductClient->reference()->list($referenceListRequest);
        $movementMethodsByValue = [];
        foreach ( $allMovementMethodsResponse->getItems() as $movementMethod){
            $movementMethodsByValue[$movementMethod->getValue()] = $movementMethod->getName();
        }

        $allObjects = $this->geAllObjects();
        $allRatingsStat = $this->getRatingsStat();

        //convert $allTourProducts to array type
        $allTourProducts = [];
        foreach ($allTourProductsResponse as $item) {

            $tourProduct=[
                "id"                  =>$item->getId(),
                "type"                =>$item->getType(),
                "object_id_list"      =>$item->getObjectIdList(),
                "lat"                 =>$item->getLat(),
                "lon"                 =>$item->getLon(),
                "edit_date_time"      =>$item->getEditDateTime(),
            ];

            if (!is_null($item->getService()))
            {
                $tourProduct["service_type"]=$item->getService()->getServiceType();
            }
            else
            {
                if (!is_null($item->getTour()))
                {
                    $tourProduct["movement_type"]=$item->getTour()->getMovementType();
                }
                else if (!is_null($item->getEvent()))
                {
                    $tourProduct["event_type"]=$item->getEvent()->getEventType();
                }
                else if (!is_null($item->getOffer()))
                {
                    $tourProduct["offer_type"]=$item->getOffer()->getOfferType();
                }
                else if (!is_null($item->getAudioguide()))
                {
                    $tourProduct["tour_type"]=$item->getAudioguide()->getTourType();
                    $tourProduct["movement_type"]=$item->getAudioguide()->getMovementType();
                }
            }

            //replace and add service fields for filtering
            if(!empty($tourProduct["type"])) {
                $tourProduct["type"] = $tourproductTypesByValue[$tourProduct["type"]];
            }
            if(!empty($tourProduct["service_type"])) {
                $tourProduct["service_type"] = $serviceTypesByValue[$tourProduct["service_type"]];
            }
            if(!empty($tourProduct["movement_type"])) {
                $tourProduct["movement_type"] = $movementMethodsByValue[$tourProduct["movement_type"]];
            }

            if(!empty($tourProduct['object_id_list'])) {
                $objectIds = $tourProduct['object_id_list'];
                if(is_array($objectIds) & count($objectIds) >= 1) {
                    $tourProduct['object_types'] = [];
                    foreach ($objectIds  as $objectId) {
                        $tourProduct['object_types'][] = $allObjects[$objectId]["object_type"];
                    }
                }
            }

            if(array_key_exists($tourProduct['id'], $allRatingsStat)) {
                $tourProduct['rating'] = $allRatingsStat[$tourProduct['id']];
            }else{
                $tourProduct['rating'] = 0;
            }

            $allTourProducts[$item->getId()] = $tourProduct;
        }

        // сортируем сразу по рейтингу и дате обновления чтобы потом сразу добавить в выборку если количество менее 'show_count'
        uasort($allTourProducts, function($a, $b){
            if ($a['rating'] < $b['rating']) {
                return 1;
            }elseif($a['rating'] > $b['rating']) {
                return -1;
            }else{
                //сортировка по edit_date_time
                if ($a['edit_date_time'] < $b['edit_date_time']) {
                    return 1;
                }elseif($a['edit_date_time'] > $b['edit_date_time']) {
                    return -1;
                }else {
                    return 0;
                }
            }
        });

        return $allTourProducts;
    }

    /**
     * @return array
     */
    private function geAllObjects ():array {

        $request = new ObjectListRequest();
        $objectListResponse=$this->objectClient->object()->list($request);
        $allObjects = [];

        $referenceListRequest=new ReferenceListRequest();
        $referenceListRequest->setType("objecttype");
        $allObjectTypesResponse = $this->objectClient->reference()->list($referenceListRequest);
        $objectTypesByValue = [];
        foreach ( $allObjectTypesResponse->getItems() as $objectType){
            $objectTypesByValue[$objectType->getValue()] = $objectType->getName();
        }

        foreach ($objectListResponse->getList() as $item) {
            $object=[
                "id"           =>$item->getId(),
                "object_type"  =>$item->getObjectType(),
            ];

            //replace int to string object_type
            if(!empty($object["object_type"])) {
                $object["object_type"] = $objectTypesByValue[ $object["object_type"] ];
            }
            $allObjects[$item->getId()] = $object;
        }
        return $allObjects;
    }

    /**
     * @return array
     */
    private function getRatingsStat():array {
        $request = new LikeRatingStatRequest();
        $ratingsStatResponse=$this->likeRatingClient->rating()->stat($request);
        $keyAsUID = [];
        foreach ($ratingsStatResponse->getList() as $rating){
            $keyAsUID[$rating->getUid()] = $rating->getRating();
        }
        return $keyAsUID;
    }






}