<?php

namespace App\Controller\Api\V1;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Validator\Validator\ValidatorInterface;
use egik\MicroserviceBundle\Response\ErrorResponse;
use egik\MicroserviceBundle\Response\MicroserviceResponse;
use egik\MicroserviceBundle\Controller\MicroserviceAbstractController;
use App\Repository\SurveyRepository;
use App\Domain\Survey\Action\Lists\ListRequest;
use App\Domain\Survey\Action\Lists\ListAction;
use App\Domain\Survey\Action\Remove\RemoveAction;
use App\Domain\Survey\Action\Save\SaveAction;
use App\Domain\Survey\Action\Save\SaveRequest;
use App\Domain\Survey\Action\History\HistoryAction;
use Exception;

class SurveyController extends MicroserviceAbstractController
{

    /**
     * @var ValidatorInterface
     */
    private $validator;

    /**
     * @var SurveyRepository
     */
    private $surveyRepository;


    public function __construct(ValidatorInterface $validator, SurveyRepository $surveyRepository)
    {
        $this->validator=$validator;
        $this->surveyRepository=$surveyRepository;
    }

    /**
     * @Route("/api/v1/survey/list", name="api.v1.survey.list", methods={"GET"})
     */
    public function listAction(ListRequest $request, ListAction $action): MicroserviceResponse
    {
        try
        {
            $violationList=$this->validator->validate($request,null,$request->getDefinedPropertiesValidatingGroups());
            if ($violationList->count()>0)
            {
                return $this->badParamsResponse("Bad params",$violationList);
            }
            return $this->successResponse($action->execute($request));
        }
        catch(Exception $exception)
        {
            return $this->errorResponse($exception->getMessage(),$exception->getCode());
        }
    }

    /**
     * @Route("/api/v1/survey/{id}", name="api.v1.survey.delete", methods={"DELETE"}, requirements={"id"="\d+"})
     */
    public function removeAction($id, RemoveAction $action): MicroserviceResponse
    {
        try
        {
            $action->execute($id);
            return $this->successResponse();
        }
        catch(Exception $exception)
        {
            return $this->errorResponse($exception->getMessage(),$exception->getCode());
        }

    }

    /**
     * @Route("/api/v1/survey/{id}", name="api.v1.survey.get", methods={"GET"}, requirements={"id"="\d+"})
     */
    public function getAction(int $id): MicroserviceResponse
    {
        try
        {
            $surveyEntity= $this->surveyRepository->find($id);
            if ($surveyEntity===null)
            {
                return $this->errorResponse("Survey not found by id=".$id,0,null,ErrorResponse::HTTP_BAD_REQUEST);
            }
            return $this->successResponse($surveyEntity);
        }
        catch(Exception $exception)
        {
            return $this->errorResponse($exception->getMessage(),$exception->getCode());
        }
    }


    /**
     * @Route("/api/v1/survey/{id}", name="api.v1.survey.save", methods={"POST"}, requirements={"id"="\d+"})
     */
    public function saveAction($id, SaveRequest $request, SaveAction $action): MicroserviceResponse
    {
        $request->setProperty("id", (int)$id);
        return $this->createAction($request, $action);
    }

    /**
     * @Route("/api/v1/survey", name="api.v1.survey.create", methods={"POST"})
     */
    public function createAction(SaveRequest $request, SaveAction $action): MicroserviceResponse
    {
        try
        {
            if (is_null($request->id)) $request->addValidatingGroup("create");
            $violationList=$this->validator->validate($request,null,$request->getDefinedPropertiesValidatingGroups());
            if ($violationList->count()>0)
            {
                return $this->badParamsResponse("Bad params",$violationList);
            }
            return $this->successResponse($action->execute($request));
        }
        catch(Exception $exception)
        {
            return $this->errorResponse($exception->getMessage(),$exception->getCode());
        }
    }

    
    /**
     * @Route("/api/v1/survey/{id}/history", name="api.v1.survey.history", methods={"GET"}, requirements={"id"="\d+"})
     */
    public function historyAction($id,HistoryAction $action): MicroserviceResponse
    {
        try
        {
            return $this->successResponse($action->execute($id));
        }
        catch(Exception $exception)
        {
            return $this->errorResponse($exception->getMessage(),$exception->getCode());
        }
    }



}
