<?php

namespace App\Controller\Api\V1;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Validator\Validator\ValidatorInterface;
use egik\MicroserviceBundle\Response\ErrorResponse;
use egik\MicroserviceBundle\Response\MicroserviceResponse;
use egik\MicroserviceBundle\Controller\MicroserviceAbstractController;
use App\Repository\PreferencesRuleRepository;
use App\Domain\PreferencesRule\Action\Lists\ListRequest;
use App\Domain\PreferencesRule\Action\Lists\ListAction;
use App\Domain\PreferencesRule\Action\Remove\RemoveAction;
use App\Domain\PreferencesRule\Action\Save\SaveAction;
use App\Domain\PreferencesRule\Action\Save\SaveRequest;
use App\Domain\PreferencesRule\Action\History\HistoryAction;
use Exception;

class PreferencesRuleController extends MicroserviceAbstractController
{

    /**
     * @var ValidatorInterface
     */
    private $validator;

    /**
     * @var PreferencesRuleRepository
     */
    private $preferencesRuleRepository;


    public function __construct(ValidatorInterface $validator, PreferencesRuleRepository $preferencesRuleRepository)
    {
        $this->validator=$validator;
        $this->preferencesRuleRepository=$preferencesRuleRepository;
    }

    /**
     * @Route("/api/v1/rule/list", name="api.v1.rule.list", methods={"GET"})
     */
    public function listAction(ListRequest $request, ListAction $action): MicroserviceResponse
    {
        try
        {
            $violationList=$this->validator->validate($request,null,$request->getDefinedPropertiesValidatingGroups());
            if ($violationList->count()>0)
            {
                return $this->badParamsResponse("Bad params",$violationList);
            }
            return $this->successResponse($action->execute($request));
        }
        catch(Exception $exception)
        {
            return $this->errorResponse($exception->getMessage(),$exception->getCode());
        }
    }

    /**
     * @Route("/api/v1/rule/{id}", name="api.v1.rule.delete", methods={"DELETE"}, requirements={"id"="\d+"})
     */
    public function removeAction($id, RemoveAction $action): MicroserviceResponse
    {
        try
        {
            $action->execute($id);
            return $this->successResponse();
        }
        catch(Exception $exception)
        {
            return $this->errorResponse($exception->getMessage(),$exception->getCode());
        }

    }

    /**
     * @Route("/api/v1/rule/{id}", name="api.v1.rule.get", methods={"GET"}, requirements={"id"="\d+"})
     */
    public function getAction(int $id): MicroserviceResponse
    {
        try
        {
            $preferencesRuleEntity= $this->preferencesRuleRepository->find($id);
            if ($preferencesRuleEntity===null)
            {
                return $this->errorResponse("PreferencesRule not found by id=".$id,0,null,ErrorResponse::HTTP_BAD_REQUEST);
            }
            return $this->successResponse($preferencesRuleEntity);
        }
        catch(Exception $exception)
        {
            return $this->errorResponse($exception->getMessage(),$exception->getCode());
        }
    }

    /**
     * @Route("/api/v1/rule/{id}", name="api.v1.rule.save", methods={"POST"}, requirements={"id"="\d+"})
     */
    public function saveAction($id, SaveRequest $request, SaveAction $action): MicroserviceResponse
    {
        $request->setProperty("id", (int)$id);
        return $this->createAction($request, $action);
    }

    /**
     * @Route("/api/v1/rule", name="api.v1.rule.create", methods={"POST"})
     */
    public function createAction(SaveRequest $request, SaveAction $action): MicroserviceResponse
    {
        try
        {
            if (is_null($request->id)) $request->addValidatingGroup("create");
            $violationList=$this->validator->validate($request,null,$request->getDefinedPropertiesValidatingGroups());
            if ($violationList->count()>0)
            {
                return $this->badParamsResponse("Bad params",$violationList);
            }
            return $this->successResponse($action->execute($request));
        }
        catch(Exception $exception)
        {
            return $this->errorResponse($exception->getMessage(),$exception->getCode());
        }
    }

    
    /**
     * @Route("/api/v1/rule/{id}/history", name="api.v1.rule.history", methods={"GET"}, requirements={"id"="\d+"})
     */
    public function historyAction($id,HistoryAction $action): MicroserviceResponse
    {
        try
        {
            return $this->successResponse($action->execute($id));
        }
        catch(Exception $exception)
        {
            return $this->errorResponse($exception->getMessage(),$exception->getCode());
        }
    }



}
