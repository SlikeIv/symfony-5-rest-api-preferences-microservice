<?php

namespace App\Controller\Api\V1;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Validator\Validator\ValidatorInterface;
use egik\MicroserviceBundle\Response\ErrorResponse;
use egik\MicroserviceBundle\Response\MicroserviceResponse;
use egik\MicroserviceBundle\Controller\MicroserviceAbstractController;
use App\Repository\SurveyResultRepository;
use App\Domain\SurveyResult\Action\Lists\ListRequest;
use App\Domain\SurveyResult\Action\Lists\ListAction;
use App\Domain\SurveyResult\Action\Save\SaveAction;
use App\Domain\SurveyResult\Action\Save\SaveRequest;
use Exception;

class SurveyResultController extends MicroserviceAbstractController
{

    /**
     * @var ValidatorInterface
     */
    private $validator;

    /**
     * @var SurveyResultRepository
     */
    private $surveyResultRepository;


    public function __construct(ValidatorInterface $validator, SurveyResultRepository $surveyResultRepository)
    {
        $this->validator=$validator;
        $this->surveyResultRepository=$surveyResultRepository;
    }

    /**
     * @Route("/api/v1/result/list", name="api.v1.result.list", methods={"GET"})
     */
    public function listAction(ListRequest $request, ListAction $action): MicroserviceResponse
    {
        try
        {
            $violationList=$this->validator->validate($request,null,$request->getDefinedPropertiesValidatingGroups());
            if ($violationList->count()>0)
            {
                return $this->badParamsResponse("Bad params",$violationList);
            }
            return $this->successResponse($action->execute($request));
        }
        catch(Exception $exception)
        {
            return $this->errorResponse($exception->getMessage(),$exception->getCode());
        }
    }

    /**
     * @Route("/api/v1/result", name="api.v1.result.create", methods={"POST"})
     */
    public function createAction(SaveRequest $request, SaveAction $action): MicroserviceResponse
    {
        try
        {
            if (is_null($request->id)) $request->addValidatingGroup("create");
            $violationList=$this->validator->validate($request,null,$request->getDefinedPropertiesValidatingGroups());
            if ($violationList->count()>0)
            {
                return $this->badParamsResponse("Bad params",$violationList);
            }
            return $this->successResponse($action->execute($request));
        }
        catch(Exception $exception)
        {
            return $this->errorResponse($exception->getMessage(),$exception->getCode());
        }
    }

}
