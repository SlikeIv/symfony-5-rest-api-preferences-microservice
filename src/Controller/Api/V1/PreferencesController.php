<?php

namespace App\Controller\Api\V1;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Validator\Validator\ValidatorInterface;
use egik\MicroserviceBundle\Response\ErrorResponse;
use egik\MicroserviceBundle\Response\MicroserviceResponse;
use egik\MicroserviceBundle\Controller\MicroserviceAbstractController;
use App\Repository\PreferencesRuleRepository;
use App\Domain\Preferences\Action\Lists\ListRequest;
use App\Domain\Preferences\Action\Lists\ListAction;
use Exception;

class PreferencesController extends MicroserviceAbstractController
{

    /**
     * @var ValidatorInterface
     */
    private $validator;

    public function __construct(ValidatorInterface $validator, PreferencesRuleRepository $preferencesRuleRepository)
    {
        $this->validator=$validator;
    }

    /**
     * @Route("/api/v1/preferences/list", name="api.v1.preferences.list", methods={"GET"})
     */
    public function listAction(ListRequest $request, ListAction $action): MicroserviceResponse
    {
        try
        {
            $violationList=$this->validator->validate($request,null,$request->getDefinedPropertiesValidatingGroups());
            if ($violationList->count()>0)
            {
                return $this->badParamsResponse("Bad params",$violationList);
            }
            return $this->successResponse($action->execute($request));
        }
        catch(Exception $exception)
        {
            return $this->errorResponse($exception->getMessage(),$exception->getCode());
        }
    }


}
