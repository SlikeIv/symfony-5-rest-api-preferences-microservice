<?php
namespace App\Command;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use App\Service\PreferencesService;

class GeneratingPreferences extends Command
{

    protected static $defaultName = 'app:generating-preferences';
    protected static $defaultDescription = 'Запуск команды генерирования предпочтений для пользователей';

    /**
     * @var PreferencesService
     */
    private $preferencesService;


    public function __construct(PreferencesService $preferencesService)
    {
        parent::__construct();
        $this->preferencesService=$preferencesService;
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $this->preferencesService->generatePreferences();
        return Command::SUCCESS;
    }

}