<?php

namespace App\Domain\Survey\Action\Save;

use egik\MicroserviceBundle\Validation\ValidationRequest;
use Symfony\Component\Validator\Constraints as Assert;

class SaveSurveyQuestionRequest extends ValidationRequest
{
    /**
     * @var int|null
     * @Assert\Positive(groups={"id"})
     */
    public $id;

    /**
     * @var string
     * @Assert\NotNull(groups={"text"})
     */
    public $text;

    /**
     * @var string|null
     */
    public $description;

    /**
     * @var bool
     * @Assert\NotNull(groups={"type"})
     * @Assert\Choice({0,1,2,3,4,5,6}, groups={"type"})
     */
    public $type;

    /**
     * @var bool
     * @Assert\NotNull(groups={"canSkip"})
     * @Assert\Choice({0,1}, groups={"canSkip"})
     */
    public $canSkip;

    /**
     * @var array
     * @Assert\NotNull(groups={"params"})
     * @Assert\Type(type="array",groups={"params"})
     */
    public $params;

    /**
     * @var int
     * @Assert\NotNull(groups={"position"})
     * @Assert\Type(type="integer", groups={"position"})
     */
    public $position;

}
