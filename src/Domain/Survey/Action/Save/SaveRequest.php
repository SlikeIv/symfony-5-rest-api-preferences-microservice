<?php

namespace App\Domain\Survey\Action\Save;

use egik\MicroserviceBundle\Validation\ValidationRequest;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Validator\Context\ExecutionContextInterface;

class SaveRequest extends ValidationRequest
{
    /**
     * @var int|null
     * @Assert\Positive(groups={"id"})
     */
    public $id;

    /**
     * @var int
     * @Assert\NotBlank(groups={"status","create"})
     * @Assert\Choice({0,1,2,3,4,5,6}, groups={"status","create"})
     */
    public $status;

    /**
     * @var string
     * @Assert\NotBlank(groups={"title","create"})
     */
    public $title;

    /**
     * @var int
     * @Assert\NotNull(groups={"isRequired","create"})
     * @Assert\Choice({0,1}, groups={"isRequired","create"})
     */
    public $isRequired;

    /**
     * @var int|null
     */
    public $repeatDays;

    /**
     * @var SaveSurveyQuestionRequest[]|null
     * @Assert\Type(type="array",groups={"surveyQuestions"})
     */
    public $surveyQuestions;


    /**
     * @Assert\Callback(groups={"surveyQuestions"})
     */
    public function validateQuestionsList(ExecutionContextInterface $context)
    {
        if (is_array($this->surveyQuestions))
        {
            foreach ($this->surveyQuestions as $idx=>$question)
            {
                $questionContext=$context->getValidator()->startContext();
                $context->getViolations()->addAll($questionContext->atPath("surveyQuestions[".$idx."]")->validate($question, null, $question->getDefinedPropertiesValidatingGroups())->getViolations());
            }
        }
    }

}
