<?php

namespace App\Domain\Survey\Action\Save;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Sensio\Bundle\FrameworkExtraBundle\Request\ParamConverter\ParamConverterInterface;
use Symfony\Component\HttpFoundation\Request;

class SaveRequestConverter implements ParamConverterInterface
{
    /**
     * @inheritdoc
     */
    public function apply(Request $request, ParamConverter $configuration)
    {
        $param = $configuration->getName();

        $dto = new SaveRequest();
        if ($request->request->has("survey")) {
            $survey = \json_decode($request->request->get("survey"), true);

            if (is_array($survey)) {
                if (array_key_exists("status", $survey)) {
                    $dto->setProperty("status", $survey["status"]);
                }

                if (array_key_exists("title", $survey)) {
                    $dto->setProperty("title", $survey["title"]);
                }

                if (array_key_exists("is_required", $survey)) {
                    $dto->setProperty("isRequired", $survey["is_required"]);
                }

                if (array_key_exists("repeat_days", $survey)) {
                    $dto->setProperty("repeatDays", $survey["repeat_days"]);
                }

                if (array_key_exists("survey_questions", $survey)) {
                    if (is_array($survey["survey_questions"])) {
                        $questions = [];
                        foreach ($survey["survey_questions"] as $question) {

                            $dtoQuestion = new SaveSurveyQuestionRequest();
                            if (array_key_exists("id", $question)) {
                                $dtoQuestion->setProperty("id", $question["id"]);
                            }

                            if (array_key_exists("text", $question)) {
                                $dtoQuestion->setProperty("text", $question["text"]);
                            }

                            if (array_key_exists("description", $question)) {
                                $dtoQuestion->setProperty("description", $question["description"]);
                            }

                            if (array_key_exists("type", $question)) {
                                $dtoQuestion->setProperty("type", $question["type"]);
                            }

                            if (array_key_exists("can_skip", $question)) {
                                $dtoQuestion->setProperty("canSkip", $question["can_skip"]);
                            }

                            if (array_key_exists("params", $question)) {
                                $dtoQuestion->setProperty("params", $question["params"]);
                            }

                            if (array_key_exists("position", $question)) {
                                $dtoQuestion->setProperty("position", $question["position"]);
                            }

                            $questions[] = $dtoQuestion;
                        }

                        $dto->setProperty("surveyQuestions", $questions);
                    } else {
                        $dto->setProperty("surveyQuestions", $survey["survey_questions"]);
                    }

                }
            }
        }

        $request->attributes->set($param, $dto);

        return true;
    }

    /**
     * @inheritdoc
     */
    public function supports(ParamConverter $configuration): bool
    {
        return $configuration->getClass() === SaveRequest::class;
    }
}
