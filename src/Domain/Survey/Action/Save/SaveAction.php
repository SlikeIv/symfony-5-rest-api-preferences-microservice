<?php

namespace App\Domain\Survey\Action\Save;

use App\Entity\Survey;
use App\Repository\SurveyRepository;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\DBAL\Exception\UniqueConstraintViolationException;
use Exception;
use App\Repository\SurveyQuestionRepository;
use App\Entity\SurveyQuestion;

class SaveAction
{
    /**
     * @var EntityManagerInterface
     */
    private $entityManager;


    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager=$entityManager;
    }

    /**
     * @param SaveRequest $request
     *
     * @return array|null
     * @throws Exception
     */
    public function execute(SaveRequest $request)
    {

        /** @var SurveyRepository $repository */
        $repository=$this->entityManager->getRepository(Survey::class);

        if (is_null($request->id))
        {
            $entity=$repository->create();
        } else {
            $entity=$repository->find($request->id);
            if (is_null($entity)) throw new Exception("Survey with id=".$request->id." not found");
        }

        if ($request->isDefined("status"))
        {
            $entity->setStatus($request->status);
        }

        if ($request->isDefined("title"))
        {
            $entity->setTitle($request->title);
        }

        if ($request->isDefined("isRequired"))
        {
            $entity->setIsRequired($request->isRequired);
        }

        if ($request->isDefined("repeatDays"))
        {
            $entity->setRepeatDays($request->repeatDays);
        }
        
        if ($request->isDefined("surveyQuestions")){
            /** @var SurveyQuestionRepository $question_repository */
            $question_repository=$this->entityManager->getRepository(SurveyQuestion::class);

            $newQuestions=[];

            foreach ($request->surveyQuestions as $question){

                if (is_null($question->id))
                {
                    $surveyQuestionEntity=$question_repository->create();
                    $surveyQuestionEntity->setSurvey($entity);
                    $entity->getSurveyQuestions()->add($surveyQuestionEntity);
                }
                else
                {
                    $surveyQuestionEntity=$question_repository->find($question->id);
                    if (is_null($surveyQuestionEntity)) throw new \Exception("SurveyQuestion with id=".$question->id." not found");

                    if ($surveyQuestionEntity->getSurvey() !== $entity){
                        throw new \Exception("Invalid SurveyQuestion id=".$question->id." not found");
                    }
                }

                $newQuestions[]=$surveyQuestionEntity;

                if ($question->isDefined("text"))
                {
                    $surveyQuestionEntity->setText($question->text);
                }
                if ($question->isDefined("description"))
                {
                    $surveyQuestionEntity->setDescription($question->description);
                }
                if ($question->isDefined("type"))
                {
                    $surveyQuestionEntity->setType($question->type);
                }
                if ($question->isDefined("canSkip"))
                {
                    $surveyQuestionEntity->setCanSkip($question->canSkip);
                }
                if ($question->isDefined("params"))
                {
                    $surveyQuestionEntity->setParams($question->params);
                }
                if ($question->isDefined("position"))
                {
                    $surveyQuestionEntity->setPosition($question->position);
                }

            }

            foreach ($entity->getSurveyQuestions() as $idx=>$surveyQuestion){
                if (!in_array($surveyQuestion,$newQuestions))
                {
                    $entity->getSurveyQuestions()->remove($idx);
                    $question_repository->remove($surveyQuestion);
                }
            }
        }


        try
        {
            $this->entityManager->flush();

        } catch(Exception $exception) {

            if ($exception instanceof UniqueConstraintViolationException)
            {
                throw new Exception("Unique constraint exception", $exception->getCode());
            }
            throw $exception;
        }

        return $request->id?null:["id"=>$entity->getId()];
    }
}
