<?php

namespace App\Domain\Survey\Action\Lists;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Sensio\Bundle\FrameworkExtraBundle\Request\ParamConverter\ParamConverterInterface;
use Symfony\Component\HttpFoundation\Request;

class ListRequestConverter implements ParamConverterInterface
{
    /**
     * @inheritdoc
     */
    public function apply(Request $request, ParamConverter $configuration)
    {
        $param = $configuration->getName();

        $dto = new ListRequest();

        if ($request->query->has("id"))
        {
            $dto->setProperty("id", $request->query->get("id"));
        }

        if ($request->query->has("title"))
        {
            $dto->setProperty("title", $request->query->get("title"));
        }

        if ($request->query->has("status"))
        {
            $dto->setProperty("status", $request->query->get("status"));
        }

        if ($request->query->has("is_required"))
        {
            $dto->setProperty("isRequired", $request->query->get("is_required"));
        }

        if ($request->query->has("repeat_days"))
        {
            $dto->setProperty("repeatDays", $request->query->get("repeat_days"));
        }

        if ($request->query->has("page"))
        {
            $dto->setProperty("page", $request->query->get("page"));
        }

        if ($request->query->has("limit"))
        {
            $dto->setProperty("limit", $request->query->get("limit"));
        }

        if ($request->query->has("sort"))
        {
            $dto->setProperty("sort", $request->query->get("sort"));
        }

        if ($request->query->has("order"))
        {
            $dto->setProperty("order", $request->query->get("order"));
        }

        $request->attributes->set($param, $dto);

        return true;
    }

    /**
     * @inheritdoc
     */
    public function supports(ParamConverter $configuration): bool
    {
        return $configuration->getClass() === ListRequest::class;
    }
}
