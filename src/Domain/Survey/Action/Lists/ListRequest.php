<?php

namespace App\Domain\Survey\Action\Lists;

use egik\MicroserviceBundle\Validation\ValidationRequest;
use Symfony\Component\Validator\Constraints as Assert;

class ListRequest extends ValidationRequest
{

    /**
     * @var int
     * @Assert\NotBlank(groups={"id"})
     * @Assert\Positive(groups={"id"})
     */
    public $id;

    /**
     * @var string
     * @Assert\NotBlank(groups={"title"})
     */
    public $title;

    /**
     * @var int
     * @Assert\NotBlank(groups={"status"})
     * @Assert\Choice({"0","1","2","3","4","5","6"}, groups={"status"})
     */
    public $status;

    /**
     * @var int
     * @Assert\NotBlank(groups={"isRequired"})
     * @Assert\Choice({"0","1"}, groups={"isRequired"})
     */
    public $isRequired;

    /**
     * @var int|null
     */
    public $repeatDays;

    /**
     * @var int|null
     * @Assert\Positive(groups={"limit"})
     */
    public $page;

    /**
     * @var int|null
     * @Assert\Positive(groups={"limit"})
     */
    public $limit;

    /**
     * @var string|null
     * @Assert\Choice(choices={"id", "title", "status", "is_required", "repeat_days"}, groups={"sort"})
     */
    public $sort;

    /**
     * @var string|null
     * @Assert\Choice(choices={"asc", "desc"}, groups={"order"})
     */
    public $order;



}
