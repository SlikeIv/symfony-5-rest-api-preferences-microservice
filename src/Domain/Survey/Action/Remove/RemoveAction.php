<?php

namespace App\Domain\Survey\Action\Remove;

use App\Entity\Survey;
use App\Repository\SurveyRepository;
use Doctrine\ORM\EntityManagerInterface;
use Exception;

class RemoveAction
{
    /**
     * @var EntityManagerInterface
     */
    private $entityManager;

    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager=$entityManager;
    }

    /**
     * @param int $id
     *
     * @throws Exception
     */
    public function execute(int $id)
    {
        /** @var SurveyRepository $repository */
        $repository=$this->entityManager->getRepository(Survey::class);
        $entity=$repository->find($id);
        if (is_null($entity)) throw new Exception("Survey exposition with id=".$id." not found");
        $repository->remove($entity);
        $this->entityManager->flush();
    }
}
