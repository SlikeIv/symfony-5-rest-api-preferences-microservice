<?php

namespace App\Domain\Survey\Action\History;

use App\Entity\Survey;
use App\Repository\SurveyRepository;
use Doctrine\ORM\EntityManagerInterface;
use Exception;
use Gedmo\Loggable\Entity\Repository\LogEntryRepository;
use egik\MicroserviceBundle\Doctrine\DoctrineExtensions\Loggable\Entity\ActionLog;

class HistoryAction
{
    /**
     * @var EntityManagerInterface
     */
    private $entityManager;

    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager=$entityManager;
    }

    /**
     * @param int $id
     *
     * @return array
     * @throws Exception
     */
    public function execute(int $id): array
    {
        /** @var SurveyRepository $repository */
        $repository=$this->entityManager->getRepository(Survey::class);
        $entity=$repository->find($id);
        if (is_null($entity)) throw new Exception("Tariff with id=".$id." not found");
        /** @var LogEntryRepository $logEntryRepository */
        $logEntryRepository=$this->entityManager->getRepository(ActionLog::class);
        return $logEntryRepository->getLogEntries($entity);
    }
}
