<?php

namespace App\Domain\Preferences\Action\Lists;

use DateTime;
use egik\MicroserviceBundle\Validation\ValidationRequest;
use Symfony\Component\Validator\Constraints as Assert;

class ListRequest extends ValidationRequest
{

    /**
     * @var int
     * @Assert\NotBlank(groups={"id"})
     * @Assert\Positive(groups={"id"})
     */
    public $id;

    /**
     * @var int
     * @Assert\NotBlank(groups={"preferencesRuleId"})
     * @Assert\Positive(groups={"preferencesRuleId"})
     */
    public $preferencesRuleId;

    /**
     * @var int
     * @Assert\NotBlank(groups={"customerId"})
     * @Assert\Positive(groups={"customerId"})
     */
    public $customerId;

    /**
     * @var DateTime
     * @Assert\Type(type="DateTime", groups={"date"})
     * @Assert\NotBlank(groups={"date"})
     */
    public $date;

    /**
     * @var int
     * @Assert\NotBlank(groups={"tourproductId"})
     * @Assert\Positive(groups={"tourproductId"})
     */
    public $tourproductId;

    /**
     * @var bool
     * @Assert\NotNull(groups={"isDeleted"})
     * @Assert\Type("bool", groups={"isDeleted"})
     */
    public $isDeleted;

    /**
     * @var bool
     * @Assert\NotNull(groups={"isConversioned"})
     * @Assert\Type("bool", groups={"isConversioned"})
     */
    public $isConversioned;

    /**
     * @var bool
     * @Assert\NotNull(groups={"isLiked"})
     * @Assert\Type("bool", groups={"isLiked"})
     */
    public $isLiked;

    /**
     * @var bool
     * @Assert\NotNull(groups={"isPurchased"})
     * @Assert\Type("bool", groups={"isPurchased"})
     */
    public $isPurchased;

    /**
     * @var int|null
     * @Assert\Positive(groups={"page"})
     */
    public $page;

    /**
     * @var int|null
     * @Assert\Positive(groups={"limit"})
     */
    public $limit;

    /**
     * @var string|null
     * @Assert\Choice(choices={"id", "preferences_rule_id", "customer_id", "date", "tourproduct_id", "is_deleted", "is_conversioned", "is_liked", "is_purchased"}, groups={"sort"})
     */
    public $sort;

    /**
     * @var string|null
     * @Assert\Choice(choices={"asc", "desc"}, groups={"order"})
     */
    public $order;



}
