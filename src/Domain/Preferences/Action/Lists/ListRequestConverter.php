<?php

namespace App\Domain\Preferences\Action\Lists;

use DateTime;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Sensio\Bundle\FrameworkExtraBundle\Request\ParamConverter\ParamConverterInterface;
use Symfony\Component\HttpFoundation\Request;

class ListRequestConverter implements ParamConverterInterface
{
    /**
     * @inheritdoc
     */
    public function apply(Request $request, ParamConverter $configuration)
    {
        $param = $configuration->getName();

        $dto = new ListRequest();

        if ($request->query->has("id"))
        {
            $dto->setProperty("id", $request->query->get("id"));
        }

        if ($request->query->has("preferences_rule_id"))
        {
            $dto->setProperty("preferencesRuleId", $request->query->get("preferences_rule_id"));
        }

        if ($request->query->has("customer_id"))
        {
            $dto->setProperty("customerId", $request->query->get("customer_id"));
        }

        if ($request->query->has("date"))
        {
            $date=$request->query->get("date") ? DateTime::createFromFormat("Y-m-d", $request->query->get("date")) : $request->query->get("date");
            $dto->setProperty("date", $date);
        }

        if ($request->query->has("tourproduct_id"))
        {
            $dto->setProperty("tourproductId", $request->query->get("tourproduct_id"));
        }

        if ($request->query->has("is_deleted"))
        {
            $isDeleted=$request->query->get("is_deleted");
            if ($isDeleted==="true"||$isDeleted==="1"){

                $isDeleted=true;
            }
            else if ($isDeleted==="false"||$isDeleted==="0")
            {
                $isDeleted=false;
            }
            $dto->setProperty("isDeleted", $isDeleted);
        }

        if ($request->query->has("is_conversioned"))
        {
            $isConversioned=$request->query->get("is_conversioned");
            if ($isConversioned==="true"||$isConversioned==="1"){
                $isConversioned=true;
            }
            else if ($isConversioned==="false"||$isConversioned==="0")
            {
                $isConversioned=false;
            }
            $dto->setProperty("isConversioned", $isConversioned);
        }

        if ($request->query->has("is_liked"))
        {
            $isLiked=$request->query->get("is_liked");
            if ($isLiked==="true"||$isLiked==="1"){
                $isLiked=true;
            }
            else if ($isLiked==="false"||$isLiked==="0")
            {
                $isLiked=false;
            }
            $dto->setProperty("isLiked", $isLiked);
        }

        if ($request->query->has("is_purchased"))
        {
            $isPurchased=$request->query->get("is_purchased");
            if ($isPurchased==="true"||$isPurchased==="1"){
                $isPurchased=true;
            }
            else if ($isPurchased==="false"||$isPurchased==="0")
            {
                $isPurchased=false;
            }
            $dto->setProperty("isPurchased", $isPurchased);
        }

        if ($request->query->has("page"))
        {
            $dto->setProperty("page", $request->query->get("page"));
        }

        if ($request->query->has("limit"))
        {
            $dto->setProperty("limit", $request->query->get("limit"));
        }

        if ($request->query->has("sort"))
        {
            $dto->setProperty("sort", $request->query->get("sort"));
        }

        if ($request->query->has("order"))
        {
            $dto->setProperty("order", $request->query->get("order"));
        }

        $request->attributes->set($param, $dto);

        return true;
    }

    /**
     * @inheritdoc
     */
    public function supports(ParamConverter $configuration): bool
    {
        return $configuration->getClass() === ListRequest::class;
    }
}
