<?php

namespace App\Domain\Preferences\Action\Lists;

use App\Entity\Preferences;
use App\Repository\PreferencesRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\String\UnicodeString;
use Exception;

class ListAction {

    /**
     * @var EntityManagerInterface
     */
    private $entityManager;

    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager=$entityManager;
    }

    /**
     * @param ListRequest $request
     *
     * @return array|null
     * @throws Exception
     */
    public function execute(ListRequest $request): ?array
    {
        /** @var PreferencesRepository $repository */
        $repository=$this->entityManager->getRepository(Preferences::class);
        $count = 0;
        $limit=null;
        $offset=null;
        $orderBy=null;
        $params = [];
        if ($request->isDefined("id")){
            $params["id"]= $request->id;
        }

        if ($request->isDefined("preferencesRuleId")){
            $params["preferencesRuleId"]=(int)$request->preferencesRuleId;
        }

        if ($request->isDefined("customerId")){
            $params["customerId"]=(int)$request->customerId;
        }

        if ($request->isDefined("date")){
            $params["date"]=$request->date ? $request->date->format("Y-m-d") : null;
        }

        if ($request->isDefined("tourproductId")){
            $params["tourproductId"]=(int)$request->tourproductId;
        }

        if ($request->isDefined("isDeleted")){
            $params["isDeleted"]=(bool)$request->isDeleted;
        }

        if ($request->isDefined("isConversioned")){
            $params["isConversioned"]=(bool)$request->isConversioned;
        }

        if ($request->isDefined("isLiked")){
            $params["isLiked"]=(bool)$request->isLiked;
        }

        if ($request->isDefined("isPurchased")){
            $params["isPurchased"]=(bool)$request->isPurchased;
        }

        if ($request->isDefined("limit"))
        {
            $limit=(int)$request->limit;
            if ($request->isDefined("page"))
            {
                $offset=($request->page-1)*$limit;
            }
        }

        if ($request->isDefined("sort"))
        {
            $sort=(new UnicodeString($request->sort))->camel()->toString();
            $orderBy=[$sort=>$request->isDefined("order")?$request->order:"asc"];
        }

        $entities=$repository->search($params, $orderBy, $limit, $offset,$count);

        return [
            "items"=>$entities??[],
            "count"=>$count
        ];
    }


}




