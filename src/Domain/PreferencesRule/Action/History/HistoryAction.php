<?php

namespace App\Domain\PreferencesRule\Action\History;

use App\Entity\PreferencesRule;
use App\Repository\PreferencesRuleRepository;
use Doctrine\ORM\EntityManagerInterface;
use Exception;
use Gedmo\Loggable\Entity\Repository\LogEntryRepository;
use egik\MicroserviceBundle\Doctrine\DoctrineExtensions\Loggable\Entity\ActionLog;

class HistoryAction
{
    /**
     * @var EntityManagerInterface
     */
    private $entityManager;

    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager=$entityManager;
    }

    /**
     * @param int $id
     *
     * @return array
     * @throws Exception
     */
    public function execute(int $id): array
    {
        /** @var PreferencesRuleRepository $repository */
        $repository=$this->entityManager->getRepository(PreferencesRule::class);
        $entity=$repository->find($id);
        if (is_null($entity)) throw new Exception("Tariff with id=".$id." not found");
        /** @var LogEntryRepository $logEntryRepository */
        $logEntryRepository=$this->entityManager->getRepository(ActionLog::class);
        return $logEntryRepository->getLogEntries($entity);
    }
}
