<?php

namespace App\Domain\PreferencesRule\Action\Lists;

use App\Entity\PreferencesRule;
use App\Repository\PreferencesRuleRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\String\UnicodeString;
use Exception;

class ListAction {

    /**
     * @var EntityManagerInterface
     */
    private $entityManager;

    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager=$entityManager;
    }

    /**
     * @param ListRequest $request
     *
     * @return array|null
     * @throws Exception
     */
    public function execute(ListRequest $request): ?array
    {
        /** @var PreferencesRuleRepository $repository */
        $repository=$this->entityManager->getRepository(PreferencesRule::class);

        $count = 0;
        $limit=null;
        $offset=null;
        $orderBy=null;
        $params = [];

        if ($request->isDefined("id")){
            $params["id"]= $request->id;
        }

        if ($request->isDefined("status")){
            $params["status"]=(int)$request->status;
        }

        if ($request->isDefined("type")){
            $params["type"]=(int)$request->type;
        }

        if ($request->isDefined("showType")){
            $params["showType"]=(int)$request->showType;
        }

        if ($request->isDefined("isActive")){
            $params["isActive"]=(int)$request->isActive;
        }

        if ($request->isDefined("isFilter")){
            $params["isFilter"]=(int)$request->isFilter;
        }

        if ($request->isDefined("isStop")){
            $params["isStop"]=(int)$request->isStop;
        }

        if ($request->isDefined("limit"))
        {
            $limit=$request->limit;
            if ($request->isDefined("page"))
            {
                $offset=($request->page-1)*$limit;
            }
        }

        if ($request->isDefined("sort"))
        {
            $sort=(new UnicodeString($request->sort))->camel()->toString();
            $orderBy=[$sort=>$request->isDefined("order")?$request->order:"asc"];
        }

        $entities=$repository->search($params, $orderBy, $limit, $offset,$count);

        return [
            "items"=>$entities??[],
            "count"=>$count
        ];
    }



}




