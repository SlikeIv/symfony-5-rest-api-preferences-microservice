<?php

namespace App\Domain\PreferencesRule\Action\Lists;

use egik\MicroserviceBundle\Validation\ValidationRequest;
use Symfony\Component\Validator\Constraints as Assert;

class ListRequest extends ValidationRequest
{

    /**
     * @var int
     * @Assert\NotBlank(groups={"id"})
     * @Assert\Positive(groups={"id"})
     */
    public $id;

    /**
     * @var int
     * @Assert\NotBlank(groups={"status"})
     * @Assert\Choice({"0","1","2","3","4","5","6"}, groups={"status"})
     */
    public $status;

    /**
     * @var int
     * @Assert\NotBlank(groups={"type"})
     * @Assert\Choice({"0","1","2","3","4","5","6","7","8","9"}, groups={"type"})
     */
    public $type;

    /**
     * @var int
     * @Assert\NotBlank(groups={"showType"})
     * @Assert\Choice({"0","1","2","3"}, groups={"showType"})
     */
    public $showType;

    /**
     * @var int
     * @Assert\NotBlank(groups={"isActive"})
     * @Assert\Choice({"0","1"}, groups={"isActive"})
     */
    public $isActive;

    /**
     * @var int
     * @Assert\NotBlank(groups={"isFilter"})
     * @Assert\Choice({"0","1"}, groups={"isFilter"})
     */
    public $isFilter;

    /**
     * @var int
     * @Assert\NotBlank(groups={"isStop"})
     * @Assert\Choice({"0","1"}, groups={"isStop"})
     */
    public $isStop;

    /**
     * @var int|null
     * @Assert\Positive(groups={"limit"})
     */
    public $page;

    /**
     * @var int|null
     * @Assert\Positive(groups={"limit"})
     */
    public $limit;

    /**
     * @var string|null
     * @Assert\Choice(choices={"id", "type", "status", "showType", "isActive", "isFilter", "isStop"}, groups={"sort"})
     */
    public $sort;

    /**
     * @var string|null
     * @Assert\Choice(choices={"asc", "desc"}, groups={"order"})
     */
    public $order;



}
