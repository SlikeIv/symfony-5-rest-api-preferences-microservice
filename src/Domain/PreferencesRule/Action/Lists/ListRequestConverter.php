<?php

namespace App\Domain\PreferencesRule\Action\Lists;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Sensio\Bundle\FrameworkExtraBundle\Request\ParamConverter\ParamConverterInterface;
use Symfony\Component\HttpFoundation\Request;

class ListRequestConverter implements ParamConverterInterface
{
    /**
     * @inheritdoc
     */
    public function apply(Request $request, ParamConverter $configuration)
    {
        $param = $configuration->getName();

        $dto = new ListRequest();

        if ($request->query->has("id"))
        {
            $dto->setProperty("id", $request->query->get("id"));
        }

        if ($request->query->has("status"))
        {
            $dto->setProperty("status", $request->query->get("status"));
        }

        if ($request->query->has("type"))
        {
            $dto->setProperty("type", $request->query->get("type"));
        }

        if ($request->query->has("show_type"))
        {
            $dto->setProperty("showType", $request->query->get("show_type"));
        }

        if ($request->query->has("is_active"))
        {
            $dto->setProperty("isActive", $request->query->get("is_active"));
        }

        if ($request->query->has("is_filter"))
        {
            $dto->setProperty("isFilter", $request->query->get("is_filter"));
        }

        if ($request->query->has("is_stop"))
        {
            $dto->setProperty("isStop", $request->query->get("is_stop"));
        }

        if ($request->query->has("page"))
        {
            $dto->setProperty("page", $request->query->get("page"));
        }

        if ($request->query->has("limit"))
        {
            $dto->setProperty("limit", $request->query->get("limit"));
        }

        if ($request->query->has("sort"))
        {
            $dto->setProperty("sort", $request->query->get("sort"));
        }

        if ($request->query->has("order"))
        {
            $dto->setProperty("order", $request->query->get("order"));
        }

        $request->attributes->set($param, $dto);

        return true;
    }

    /**
     * @inheritdoc
     */
    public function supports(ParamConverter $configuration): bool
    {
        return $configuration->getClass() === ListRequest::class;
    }
}
