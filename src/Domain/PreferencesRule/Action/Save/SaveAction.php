<?php

namespace App\Domain\PreferencesRule\Action\Save;

use App\Entity\PreferencesRule;
use App\Repository\PreferencesRuleRepository;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\DBAL\Exception\UniqueConstraintViolationException;
use Exception;

class SaveAction
{
    /**
     * @var EntityManagerInterface
     */
    private $entityManager;


    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager=$entityManager;
    }

    /**
     * @param SaveRequest $request
     *
     * @return array|null
     * @throws Exception
     */
    public function execute(SaveRequest $request)
    {

        /** @var PreferencesRuleRepository $repository */
        $repository=$this->entityManager->getRepository(PreferencesRule::class);

        if (is_null($request->id))
        {
            $entity=$repository->create();
        } else {
            $entity=$repository->find($request->id);
            if (is_null($entity)) throw new Exception("PreferencesRule with id=".$request->id." not found");
        }

        if ($request->isDefined("status"))
        {
            $entity->setStatus($request->status);
        }

        if ($request->isDefined("type"))
        {
            $entity->setType($request->type);
        }

        if ($request->isDefined("minCount"))
        {
            $entity->setMinCount($request->minCount);
        }

        if ($request->isDefined("minRating"))
        {
            $entity->setMinRating($request->minRating);
        }

        if ($request->isDefined("productType"))
        {
            $entity->setProductType($request->productType);
        }

        if ($request->isDefined("showCount"))
        {
            $entity->setShowCount($request->showCount);
        }

        if ($request->isDefined("showType"))
        {
            $entity->setShowType($request->showType);
        }


        if ($request->isDefined("dateStart"))
        {
            $entity->setDateStart($request->dateStart);
        }

        if ($request->isDefined("dateEnd"))
        {
            $entity->setDateEnd($request->dateEnd);
        }

        if ($request->isDefined("isActive"))
        {
            $entity->setIsActive($request->isActive);
        }

        if ($request->isDefined("isFilter"))
        {
            $entity->setIsFilter($request->isFilter);
        }

        if ($request->isDefined("isStop"))
        {
            $entity->setIsStop($request->isStop);
        }

        try
        {
            $this->entityManager->flush();

        } catch(Exception $exception) {

            if ($exception instanceof UniqueConstraintViolationException)
            {
                throw new Exception("Unique constraint exception", $exception->getCode());
            }
            throw $exception;
        }

        return $request->id?null:["id"=>$entity->getId()];
    }
}
