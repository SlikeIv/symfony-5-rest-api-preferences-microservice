<?php

namespace App\Domain\PreferencesRule\Action\Save;

use DateTime;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Sensio\Bundle\FrameworkExtraBundle\Request\ParamConverter\ParamConverterInterface;
use Symfony\Component\HttpFoundation\Request;

class SaveRequestConverter implements ParamConverterInterface
{
    /**
     * @inheritdoc
     */
    public function apply(Request $request, ParamConverter $configuration)
    {
        $param = $configuration->getName();

        $dto = new SaveRequest();
        if ($request->request->has("rule")) {
            $rule = \json_decode($request->request->get("rule"), true);

            if (is_array($rule)) {
                if (array_key_exists("status", $rule)) {
                    $dto->setProperty("status", $rule["status"]);
                }

                if (array_key_exists("type", $rule)) {
                    $dto->setProperty("type", $rule["type"]);
                }

                if (array_key_exists("min_count", $rule)) {
                    $dto->setProperty("minCount", $rule["min_count"]);
                }

                if (array_key_exists("min_rating", $rule)) {
                    $dto->setProperty("minRating", $rule["min_rating"]);
                }

                if (array_key_exists("product_type", $rule)) {
                    $dto->setProperty("productType", $rule["product_type"]);
                }

                if (array_key_exists("show_count", $rule)) {
                    $dto->setProperty("showCount", $rule["show_count"]);
                }

                if (array_key_exists("show_type", $rule)) {
                    $dto->setProperty("showType", $rule["show_type"]);
                }

                if (array_key_exists("date_start", $rule)) {
                    $dateStart=$rule["date_start"] ? DateTime::createFromFormat("Y-m-d", $rule["date_start"]) : $rule["date_start"];
                    $dto->setProperty("dateStart", $dateStart);
                }

                if (array_key_exists("date_end", $rule)) {
                    $dateEnd=$rule["date_end"] ? DateTime::createFromFormat("Y-m-d", $rule["date_end"]) : $rule["date_end"];
                    $dto->setProperty("dateEnd", $dateEnd);
                }

                if (array_key_exists("is_active", $rule)) {
                    $dto->setProperty("isActive", $rule["is_active"]);
                }

                if (array_key_exists("is_filter", $rule)) {
                    $dto->setProperty("isFilter", $rule["is_filter"]);
                }

                if (array_key_exists("is_stop", $rule)) {
                    $dto->setProperty("isStop", $rule["is_stop"]);
                }

            }
        }

        $request->attributes->set($param, $dto);

        return true;
    }

    /**
     * @inheritdoc
     */
    public function supports(ParamConverter $configuration): bool
    {
        return $configuration->getClass() === SaveRequest::class;
    }
}
