<?php

namespace App\Domain\PreferencesRule\Action\Save;

use DateTime;
use egik\MicroserviceBundle\Validation\ValidationRequest;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Validator\Context\ExecutionContextInterface;

class SaveRequest extends ValidationRequest
{
    /**
     * @var int|null
     * @Assert\Positive(groups={"id"})
     */
    public $id;

    /**
     * @var int
     * @Assert\NotBlank(groups={"status","create"})
     * @Assert\Choice({0,1,2,3,4,5,6}, groups={"status","create"})
     */
    public $status;

    /**
     * @var int
     * @Assert\NotBlank(groups={"type","create"})
     * @Assert\Choice({0,1,2,3,4,5,6,7,8,9}, groups={"type","create"})
     */
    public $type;

    /**
     * @var int
     * @Assert\NotBlank(groups={"minCount","create"})
     * @Assert\Positive(groups={"minCount","create"})
     */
    public $minCount;

    /**
     * @var int
     * @Assert\NotBlank(groups={"minRating","create"})
     * @Assert\Positive(groups={"minRating","create"})
     */
    public $minRating;

    /**
     * @var int|null
     */
    public $productType;

    /**
     * @var int
     * @Assert\NotBlank(groups={"showCount","create"})
     * @Assert\Positive(groups={"showCount","create"})
     */
    public $showCount;

    /**
     * @var int
     * @Assert\NotBlank(groups={"showType","create"})
     * @Assert\Choice({0,1,2,3}, groups={"showType","create"})
     */
    public $showType;

    /**
     * @var DateTime
     * @Assert\NotBlank(groups={"dateStart","create"})
     */
    public $dateStart;

    /**
     * @var DateTime|null
     */
    public $dateEnd;

    /**
     * @var int
     * @Assert\NotBlank(groups={"isActive","create"})
     * @Assert\Choice({0,1}, groups={"isActive","create"})
     */
    public $isActive;

    /**
     * @var int
     * @Assert\NotBlank(groups={"isFilter","create"})
     * @Assert\Choice({0,1}, groups={"isFilter","create"})
     */
    public $isFilter;

    /**
     * @var int
     * @Assert\NotBlank(groups={"isStop","create"})
     * @Assert\Choice({0,1}, groups={"isStop","create"})
     */
    public $isStop;




}
