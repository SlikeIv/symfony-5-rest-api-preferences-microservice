<?php

namespace App\Domain\PreferencesRule\Action\Remove;

use App\Entity\PreferencesRule;
use App\Repository\PreferencesRuleRepository;
use Doctrine\ORM\EntityManagerInterface;
use Exception;

class RemoveAction
{
    /**
     * @var EntityManagerInterface
     */
    private $entityManager;

    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager=$entityManager;
    }

    /**
     * @param int $id
     *
     * @throws Exception
     */
    public function execute(int $id)
    {
        /** @var PreferencesRuleRepository $repository */
        $repository=$this->entityManager->getRepository(PreferencesRule::class);
        $entity=$repository->find($id);
        if (is_null($entity)) throw new Exception("PreferencesRule exposition with id=".$id." not found");
        $repository->remove($entity);
        $this->entityManager->flush();
    }
}
