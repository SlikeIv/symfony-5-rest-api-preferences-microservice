<?php

namespace App\Domain\SurveyResult\Action\Lists;

use DateTime;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Sensio\Bundle\FrameworkExtraBundle\Request\ParamConverter\ParamConverterInterface;
use Symfony\Component\HttpFoundation\Request;

class ListRequestConverter implements ParamConverterInterface
{
    /**
     * @inheritdoc
     */
    public function apply(Request $request, ParamConverter $configuration)
    {
        $param = $configuration->getName();

        $dto = new ListRequest();

        if ($request->query->has("id"))
        {
            $dto->setProperty("id", $request->query->get("id"));
        }

        if ($request->query->has("survey_id"))
        {
            $dto->setProperty("surveyId", $request->query->get("survey_id"));
        }


        if ($request->query->has("customer_id"))
        {
            $dto->setProperty("customerId", $request->query->get("customer_id"));
        }

        if ($request->query->has("status"))
        {
            $dto->setProperty("status", $request->query->get("status"));
        }

        if ($request->query->has("survey_date"))
        {
            $surveyDate=$request->query->get("survey_date") ? DateTime::createFromFormat("Y-m-d", $request->query->get("survey_date")) : $request->query->get("survey_date");
            $dto->setProperty("surveyDate", $surveyDate);
        }

        if ($request->query->has("page"))
        {
            $dto->setProperty("page", $request->query->get("page"));
        }

        if ($request->query->has("limit"))
        {
            $dto->setProperty("limit", $request->query->get("limit"));
        }

        if ($request->query->has("sort"))
        {
            $dto->setProperty("sort", $request->query->get("sort"));
        }

        if ($request->query->has("order"))
        {
            $dto->setProperty("order", $request->query->get("order"));
        }

        $request->attributes->set($param, $dto);

        return true;
    }

    /**
     * @inheritdoc
     */
    public function supports(ParamConverter $configuration): bool
    {
        return $configuration->getClass() === ListRequest::class;
    }
}
