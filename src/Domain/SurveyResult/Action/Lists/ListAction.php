<?php

namespace App\Domain\SurveyResult\Action\Lists;

use App\Entity\SurveyResult;
use App\Repository\SurveyResultRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\String\UnicodeString;
use Exception;

class ListAction {

    /**
     * @var EntityManagerInterface
     */
    private $entityManager;

    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager=$entityManager;
    }

    /**
     * @param ListRequest $request
     *
     * @return array|null
     * @throws Exception
     */
    public function execute(ListRequest $request): ?array
    {
        /** @var SurveyResultRepository $repository */
        $repository=$this->entityManager->getRepository(SurveyResult::class);

        $count = 0;
        $limit=null;
        $offset=null;
        $orderBy=null;
        $params = [];

        if ($request->isDefined("id")){
            $params["id"]= $request->id;
        }

        if ($request->isDefined("surveyId")){
            $params["surveyId"]=(int)$request->surveyId;
        }

        if ($request->isDefined("customerId")){
            $params["customerId"]=(int)$request->customerId;
        }

        if ($request->isDefined("status")){
            $params["status"]=(int)$request->status;
        }

        if ($request->isDefined("surveyDate")){
            $params["surveyDate"]=$request->surveyDate;
        }

        if ($request->isDefined("limit"))
        {
            $limit=$request->limit;
            if ($request->isDefined("page"))
            {
                $offset=($request->page-1)*$limit;
            }
        }

        if ($request->isDefined("sort"))
        {
            $sort=(new UnicodeString($request->sort))->camel()->toString();
            $orderBy=[$sort=>$request->isDefined("order")?$request->order:"asc"];
        }

        $entities=$repository->search($params, $orderBy, $limit, $offset,$count);

        return [
            "items"=>$entities??[],
            "count"=>$count
        ];
    }


}




