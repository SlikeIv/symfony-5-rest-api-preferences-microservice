<?php

namespace App\Domain\SurveyResult\Action\Lists;

use DateTime;
use egik\MicroserviceBundle\Validation\ValidationRequest;
use Symfony\Component\Validator\Constraints as Assert;

class ListRequest extends ValidationRequest
{

    /**
     * @var int
     * @Assert\NotBlank(groups={"id"})
     * @Assert\Positive(groups={"id"})
     */
    public $id;

    /**
     * @var int
     * @Assert\NotBlank(groups={"surveyId"})
     * @Assert\Positive(groups={"surveyId"})
     */
    public $surveyId;

    /**
     * @var int
     * @Assert\NotBlank(groups={"customerId"})
     * @Assert\Positive(groups={"customerId"})
     */
    public $customerId;

    /**
     * @var int
     * @Assert\NotBlank(groups={"status"})
     * @Assert\Choice({"0","1","2","3"}, groups={"status"})
     */
    public $status;

    /**
     * @var DateTime
     * @Assert\NotBlank(groups={"surveyDate"})
     */
    public $surveyDate;

    /**
     * @var int|null
     * @Assert\Positive(groups={"limit"})
     */
    public $page;

    /**
     * @var int|null
     * @Assert\Positive(groups={"limit"})
     */
    public $limit;

    /**
     * @var string|null
     * @Assert\Choice(choices={"id", "title", "status", "is_required", "repeat_days"}, groups={"sort"})
     */
    public $sort;

    /**
     * @var string|null
     * @Assert\Choice(choices={"asc", "desc"}, groups={"order"})
     */
    public $order;



}
