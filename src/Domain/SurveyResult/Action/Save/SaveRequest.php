<?php

namespace App\Domain\SurveyResult\Action\Save;

use DateTime;
use egik\MicroserviceBundle\Validation\ValidationRequest;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Validator\Context\ExecutionContextInterface;

class SaveRequest extends ValidationRequest
{
    /**
     * @var int|null
     * @Assert\Positive(groups={"id"})
     */
    public $id;

    /**
     * @var int
     * @Assert\NotBlank(groups={"surveyId","create"})
     * @Assert\Positive(groups={"surveyId","create"})
     */
    public $surveyId;

    /**
     * @var int
     * @Assert\NotBlank(groups={"customerId","create"})
     * @Assert\Positive(groups={"customerId","create"})
     */
    public $customerId;

    /**
     * @var DateTime
     * @Assert\NotBlank(groups={"surveyDateTime","create"})
     */
    public $surveyDateTime;

    /**
     * @var int
     * @Assert\NotBlank(groups={"status","create"})
     * @Assert\Choice({0,1,2,3}, groups={"status","create"})
     */
    public $status;

    /**
     * @var DateTime|null
     */
    public $processingDateTime;

    /**
     * @var SaveSurveyResultAnswerRequest[]|null
     * @Assert\Type(type="array", groups={"surveyResultAnswers"})
     */
    public $surveyResultAnswers;


    /**
     * @Assert\Callback(groups={"surveyResultAnswers"})
     */
    public function validateQuestionsList(ExecutionContextInterface $context)
    {
        if (is_array($this->surveyResultAnswers))
        {
            foreach ($this->surveyResultAnswers as $idx=>$answer)
            {
                $questionContext=$context->getValidator()->startContext();
                $context->getViolations()->addAll($questionContext->atPath("surveyResultAnswers[".$idx."]")->validate($answer, null, $answer->getDefinedPropertiesValidatingGroups())->getViolations());
            }
        }
    }

}
