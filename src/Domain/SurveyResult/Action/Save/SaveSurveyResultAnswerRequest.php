<?php

namespace App\Domain\SurveyResult\Action\Save;

use egik\MicroserviceBundle\Validation\ValidationRequest;
use Symfony\Component\Validator\Constraints as Assert;

class SaveSurveyResultAnswerRequest extends ValidationRequest
{
    /**
     * @var int|null
     * @Assert\Positive(groups={"id"})
     */
    public $id;

    /**
     * @var int
     * @Assert\NotNull(groups={"questionId"})
     * @Assert\Positive(groups={"questionId"})
     */
    public $questionId;

    /**
     * @var array
     * @Assert\NotNull(groups={"answer"})
     * @Assert\Type(type="array",groups={"answer"})
     */
    public $answer;


}
