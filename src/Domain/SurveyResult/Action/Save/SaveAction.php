<?php

namespace App\Domain\SurveyResult\Action\Save;

use App\Entity\SurveyResult;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\DBAL\Exception\UniqueConstraintViolationException;
use Exception;
use App\Repository\SurveyResultAnswersRepository;
use App\Entity\SurveyResultAnswers;
use App\Entity\Survey;
use App\Repository\SurveyResultRepository;
use App\Entity\SurveyQuestion;

class SaveAction
{
    /**
     * @var EntityManagerInterface
     */
    private $entityManager;


    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager=$entityManager;
    }

    /**
     * @param SaveRequest $request
     *
     * @return array|null
     * @throws Exception
     */
    public function execute(SaveRequest $request)
    {

        /** @var SurveyResultRepository $repository */
        $repository=$this->entityManager->getRepository(SurveyResult::class);

        if (is_null($request->id))
        {
            $entity=$repository->create();
        } else {
            $entity=$repository->find($request->id);
            if (is_null($entity)) throw new Exception("SurveyResult with id=".$request->id." not found");
        }

        if ($request->isDefined("surveyId"))
        {
            $surveyRepository=$this->entityManager->getRepository(Survey::class);
            /** @var Survey $surveyEntity */
            $surveyEntity=$surveyRepository->find($request->surveyId);
            if (is_null($surveyEntity)){
                throw new Exception("Survey with id=".$request->surveyId." not found");
            }
            $entity->setSurvey($surveyEntity);
        }

        if ($request->isDefined("customerId"))
        {
            $entity->setCustomerId($request->customerId);
        }

        if ($request->isDefined("surveyDateTime"))
        {
            $entity->setSurveyDateTime($request->surveyDateTime);
        }

        if ($request->isDefined("status"))
        {
            $entity->setStatus($request->status);
        }

        if ($request->isDefined("processingDateTime"))
        {
            $entity->setProcessingDateTime($request->processingDateTime);
        }
        
        if ($request->isDefined("surveyResultAnswers")){
            /** @var SurveyResultAnswersRepository $answer_repository */
            $answer_repository=$this->entityManager->getRepository(SurveyResultAnswers::class);

            $questionRepository=$this->entityManager->getRepository(SurveyQuestion::class);

            $newAnswers=[];

            foreach ($request->surveyResultAnswers as $answer){

                if (is_null($answer->id))
                {
                    $surveyAnswerEntity=$answer_repository->create();
                    $surveyAnswerEntity->setSurveyResult($entity);
                    $entity->getSurveyResultAnswers()->add($surveyAnswerEntity);
                }
                else
                {
                    $surveyAnswerEntity=$answer_repository->find($answer->id);
                    if (is_null($surveyAnswerEntity)) throw new \Exception("SurveyResultAnswers with id=".$answer->id." not found");

                    if ($surveyAnswerEntity->getSurveyResult() !== $entity){
                        throw new \Exception("Invalid SurveyResultAnswers id=".$answer->id." not found");
                    }
                }

                $newAnswers[]=$surveyAnswerEntity;

                if ($answer->isDefined("questionId"))
                {
                    /** @var SurveyQuestion $surveyQuestionEntity */
                    $surveyQuestionEntity=$questionRepository->find($answer->questionId);
                    if (is_null($surveyQuestionEntity)){
                        throw new Exception("SurveyQuestion with id=".$answer->questionId." not found");
                    }
                    $surveyAnswerEntity->setQuestion($surveyQuestionEntity);
                }

                if ($answer->isDefined("answer"))
                {
                    $surveyAnswerEntity->setAnswer($answer->answer);
                }

            }

            foreach ($entity->getSurveyResultAnswers() as $idx=>$surveyResultAnswer){
                if (!in_array($surveyResultAnswer, $newAnswers))
                {
                    $entity->getSurveyResultAnswers()->remove($idx);
                    $answer_repository->remove($surveyResultAnswer);
                }
            }
        }

        try
        {
            $this->entityManager->flush();

        } catch(Exception $exception) {

            if ($exception instanceof UniqueConstraintViolationException)
            {
                throw new Exception("Unique constraint exception", $exception->getCode());
            }
            throw $exception;
        }

        return $request->id?null:["id"=>$entity->getId()];
    }
}
