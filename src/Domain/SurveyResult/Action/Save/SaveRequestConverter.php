<?php

namespace App\Domain\SurveyResult\Action\Save;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Sensio\Bundle\FrameworkExtraBundle\Request\ParamConverter\ParamConverterInterface;
use Symfony\Component\HttpFoundation\Request;
use DateTime;

class SaveRequestConverter implements ParamConverterInterface
{
    /**
     * @inheritdoc
     */
    public function apply(Request $request, ParamConverter $configuration)
    {
        $param = $configuration->getName();

        $dto = new SaveRequest();
        if ($request->request->has("survey_result")) {
            $surveyResult = \json_decode($request->request->get("survey_result"), true);

            if (is_array($surveyResult)) {

                if (array_key_exists("survey_id", $surveyResult)) {
                    $dto->setProperty("surveyId", $surveyResult["survey_id"]);
                }

                if (array_key_exists("customer_id", $surveyResult)) {
                    $dto->setProperty("customerId", $surveyResult["customer_id"]);
                }

                if (array_key_exists("survey_date_time", $surveyResult)) {
                    $surveyDateTime=$surveyResult["survey_date_time"] ? DateTime::createFromFormat("Y-m-d H:i:s", $surveyResult["survey_date_time"]) : $surveyResult["survey_date_time"];
                    $dto->setProperty("surveyDateTime",$surveyDateTime);
                }

                if (array_key_exists("status", $surveyResult)) {
                    $dto->setProperty("status", $surveyResult["status"]);
                }

                if (array_key_exists("processing_date_time", $surveyResult)) {
                    $processingDateTime=$surveyResult["processing_date_time"] ? DateTime::createFromFormat("Y-m-d H:i:s", $surveyResult["processing_date_time"]) : $surveyResult["processing_date_time"];
                    $dto->setProperty("processingDateTime", $processingDateTime);
                }

                if (array_key_exists("survey_result_answers", $surveyResult)) {
                    if (is_array($surveyResult["survey_result_answers"])) {
                        $answers = [];
                        foreach ($surveyResult["survey_result_answers"] as $answer) {

                            $dtoAnswer = new SaveSurveyResultAnswerRequest();
                            if (array_key_exists("id", $answer)) {
                                $dtoAnswer->setProperty("id", $answer["id"]);
                            }

                            if (array_key_exists("question_id", $answer)) {
                                $dtoAnswer->setProperty("questionId", $answer["question_id"]);
                            }

                            if (array_key_exists("answer", $answer)) {
                                $dtoAnswer->setProperty("answer", $answer["answer"]);
                            }

                            $answers[] = $dtoAnswer;
                        }

                        $dto->setProperty("surveyResultAnswers", $answers);
                    } else {
                        $dto->setProperty("surveyResultAnswers", $surveyResult["survey_result_answers"]);
                    }

                }
            }
        }

        $request->attributes->set($param, $dto);

        return true;
    }

    /**
     * @inheritdoc
     */
    public function supports(ParamConverter $configuration): bool
    {
        return $configuration->getClass() === SaveRequest::class;
    }
}
