<?php

namespace App\DataFixtures;

use App\Entity\Reference\ProcessingType;
use App\Entity\Reference\Remoteness;
use App\Entity\Reference\Season;
use App\Entity\Reference\TimesOfDay;
use App\Entity\Reference\ServiceMethod;
use App\Entity\Reference\ChildrenCondition;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use egik\MicroserviceBundle\Repository\ReferenceRepository;
use egik\MicroserviceBundle\Entity\Reference;

class ReferenceCommonFixtures extends Fixture
{
	/**
	 * @var ParameterBagInterface
	 */
	private $parameterBag;

	public function __construct(ParameterBagInterface $parameterBag)
	{
		$this->parameterBag = $parameterBag;
	}

	/**
	 * @param ObjectManager $manager
	 *
	 * @throws \Doctrine\ORM\NonUniqueResultException
	 */
	public function load(ObjectManager $manager)
	{
  		/** @var ReferenceRepository $referenceRepository */
  		$referenceRepository=$manager->getRepository(Reference::class);
  		$refLists=array_merge(
  			$this->loadProcessingType(),
  			$this->loadRemoteness(),
  			$this->loadSeason(),
  			$this->loadTimesOfDay(),
  			$this->loadServiceMethod(),
  			$this->loadChildrenCondition(),
		  );

  		foreach ($refLists as $refClass=>$refList)
  		{
        $parent = null;
  			foreach ($refList as $refItem)
  			{
  				$reference=$referenceRepository->findOneByType($refClass, $refItem["name"]);
  				if (is_null($reference))
  				{
  					/* @var Reference $reference*/
  					$reference=new $refClass();
  					$manager->persist($reference);
  					$reference->setName($refItem["name"]);
  				}
  				$reference->setValue($refItem["value"]);
  				$reference->setDescription($refItem["desc"]);
  			}
  		}
  		$manager->flush();
	}

	private function loadProcessingType(): array
	{
		return [
			ProcessingType::class=>[
				[
					"name"=>"tourproducttype",
					"value"=>1,
					"desc"=>"Предпочитаемые турпродукты"
				],
				[
					"name"=>"objecttype",
					"value"=>2,
					"desc"=>"Предпочитаемые геообъекты"
				],
				[
					"name"=>"servicetype",
					"value"=>3,
					"desc"=>"Предпочитаемые услуги"
				],
				[
					"name"=>"date",
					"value"=>4,
					"desc"=>"Предпочитаемые по датам"
				],
				[
					"name"=>"season",
					"value"=>5,
					"desc"=>"Предпочитаемые сезон"
				],
				[
					"name"=>"timesofday",
					"value"=>6,
					"desc"=>"Предпочитаемое время суток"
				],
				[
					"name"=>"childrencondition",
					"value"=>7,
					"desc"=>"Условия по детям"
				],
				[
					"name"=>"movementmethod",
					"value"=>8,
					"desc"=>"Предпочитаемые способы перемещения"
				],
				[
					"name"=>"servicemethod",
					"value"=>9,
					"desc"=>"Предпочитаемый уровень обслуживания"
				],
				[
					"name"=>"remoteness",
					"value"=>10,
					"desc"=>"Предпочитаемая удаленность"
				],
			]
		];
	}

	private function loadRemoteness(): array
	{
		return [
			Remoteness::class=>[
				[
					"name"=>"inKaliningrad",
					"value"=>1,
					"desc"=>"В Калининграде"
				],
				[
					"name"=>"outsideKaliningrad",
					"value"=>2,
					"desc"=>"За его пределами"
				]
			]
		];
	}

	private function loadSeason(): array
	{
		return [
			Season::class=>[
				[
					"name"=>"winter",
					"value"=>1,
					"desc"=>"Зима"
				],
				[
					"name"=>"spring",
					"value"=>2,
					"desc"=>"Весна"
				],
				[
					"name"=>"summer",
					"value"=>3,
					"desc"=>"Лето"
				],
				[
					"name"=>"autumn",
					"value"=>4,
					"desc"=>"Осень"
				],
				[
					"name"=>"anySeason",
					"value"=>5,
					"desc"=>"Любое время года"
				]
			]
		];
	}

	private function loadTimesOfDay(): array
	{
		return [
			TimesOfDay::class=>[
				[
					"name"=>"morning",
					"value"=>1,
					"desc"=>"Утро"
				],
				[
					"name"=>"day",
					"value"=>2,
					"desc"=>"День"
				],
				[
					"name"=>"evening",
					"value"=>3,
					"desc"=>"Вечер"
				],
				[
					"name"=>"night",
					"value"=>4,
					"desc"=>"Ночь"
				],
				[
					"name"=>"anyTime",
					"value"=>5,
					"desc"=>"Любое время суток"
				]
			]
		];
	}

  private function loadServiceMethod(): array
  {
    return [
      ServiceMethod::class=>[
        [
          "name"=>"group",
          "value"=>1,
          "desc"=>"Групповой"
        ],
        [
          "name"=>"individually",
          "value"=>2,
          "desc"=>"Индивидуально"
        ]
      ]
    ];
  }


  private function loadChildrenCondition(): array
  {
    return [
      ChildrenCondition::class=>[
        [
          "name"=>"withСhildren",
          "value"=>1,
          "desc"=>"С детьми"
        ],
        [
          "name"=>"withoutСhildren",
          "value"=>2,
          "desc"=>"Без детей"
        ]
      ]
    ];
  }

}
