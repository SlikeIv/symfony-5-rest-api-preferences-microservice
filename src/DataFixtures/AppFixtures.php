<?php

namespace App\DataFixtures;


use App\Entity\Survey;
use App\Entity\SurveyQuestion;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;

class AppFixtures extends Fixture implements  DependentFixtureInterface
{

	public function getDependencies()
	{
		return [
			ReferenceCommonFixtures::class,
		];
	}

	/**
	 * @param SurveyQuestion[] $questionList
	 * @param array $question
	 * @return SurveyQuestion|null
	 */
  private function getExistQuestionInSurvey(array $questionList, array $question): ?SurveyQuestion {
    foreach ($questionList as $existQuestion) {
        if($existQuestion->getParams()['processing_type'] == $question['params']['processing_type'] ) {
          return $existQuestion;
        }
    }
    return null;
  }

	/**
	 * @param array $defaultQuestions
	 * @param SurveyQuestion $existQuestion
	 * @return bool
	 */
  private function existInDefaultQuestions(array $defaultQuestions, SurveyQuestion $existQuestion):bool {
    foreach ($defaultQuestions as $question) {
        if($question['params']['processing_type'] == $existQuestion->getParams()['processing_type']) {
          return true;
        }
    }
    return false;
  }

	/**
	 * @return array
	 */
  private function getDefaultQuestions(): array {
      $questionList = [
        //Что?
        [
          "text"=>"Ваши предпочтения?",
          "type"=>3,
          "description"=> null,
          "can_skip"=>1,
          "params"=>[
            "reference" => [
               "type" => "tourproducttype",
               "service" => "tourproduct"
             ],
						 "processing_type" => "tourproducttype"
          ],
          "position"=>1,
        ],
        [
          "text"=>"Какие достопримечательности хотите увидеть?",
          "type"=>3,
          "description"=> null,
          "can_skip"=>1,
          "params"=>[
            "reference" => [
               "type" => "objecttype",
               "service" => "object"
             ],
						 "processing_type" => "objecttype"
          ],
          "position"=>2,
        ],
        [
          "text"=>"Какие услуги вас интересуют?",
          "type"=>3,
          "description"=> null,
          "can_skip"=>1,
          "params"=>[
            "reference" => [
               "type" => "servicetype",
               "service" => "tourproduct"
             ],
						 "processing_type" => "servicetype"
          ],
          "position"=>3,
        ],
        // Когда?
        [
          "text"=>"Поиск предпочтений по конкретным или плавающим датам?",
          "type"=>6,
          "description"=> null,
          "can_skip"=>1,
          "params"=>[
						"processing_type" => "date"
          ],
          "position"=>4,
        ],
        [
          "text"=>"Какое время года? Чем заняться зимой/весной/летом/осенью?",
          "type"=>3,
          "description"=> null,
          "can_skip"=>1,
          "params"=>[
            "reference" => [
               "type" => "season",
               "service" => "preferences"
             ],
						 	"processing_type" => "season"
          ],
          "position"=>5,
        ],
        [
          "text"=>"Какое время суток?",
          "type"=>3,
          "description"=> null,
          "can_skip"=>1,
          "params"=>[
            "reference" => [
               "type" => "timesofday",
               "service" => "preferences"
             ],
						 "processing_type" => "timesofday"
          ],
          "position"=>6,
        ],
        //Как?
        [
          "text"=>"С детьми?",
          "type"=>3,
          "description"=> null,
          "can_skip"=>1,
          "params"=>[
            "reference" => [
               "type" => "childrencondition",
               "service" => "preferences"
             ],
						 "processing_type" => "childrencondition"
          ],
          "position"=>7,
        ],
        [
          "text"=>"На чем удобнее перемещаться?",
          "type"=>3,
          "description"=> null,
          "can_skip"=>1,
          "params"=>[
            "reference" => [
               "type" => "movementmethod",
               "service" => "tourproduct"
             ],
						 	"processing_type" => "movementmethod"
          ],
          "position"=>8,
        ],
        [
          "text"=>"Индивидуально или с группой?",
          "type"=>3,
          "description"=> null,
          "can_skip"=>1,
          "params"=>[
            "reference" => [
               "type" => "servicemethod",
               "service" => "preferences"
             ],
						 	"processing_type" => "servicemethod"
          ],
          "position"=>9,
        ],
        // Где?
        [
          "text"=>"Хотите посетить/посмотреть места в Калининграде или за его пределом",
          "type"=>4,
          "description"=> null,
          "can_skip"=>1,
          "params"=>[
            "reference" => [
               "type" => "remoteness",
               "service" => "preferences"
             ],
						 "processing_type" => "remoteness"
          ],
          "position"=>10,
        ],
      ];

      return $questionList;
  }

	public function load(ObjectManager $manager)
	{
  		$surveyRepository=$manager->getRepository(Survey::class);
  		$surveyList=$surveyRepository->findBy([], ["id"=>"asc"], 1);
  		$survey=count($surveyList) > 0 ? $surveyList[0] : null;

    	if (is_null($survey))
  		{
  			$survey=new Survey();
  			$manager->persist($survey);
  		  $survey->setTitle("Ваши предпочтения");
  		  $survey->setStatus(0);
  		  $survey->setIsRequired(0);
  		  $survey->setRepeatDays(null);
        $manager->flush();
  		}

      $questionRepository=$manager->getRepository(SurveyQuestion::class);
      $questionList=$questionRepository->findBy(["survey" => 	$survey->getId() ], ["position"=>"asc"]);

      // insert new or update exists questions
      $defaultQuestions = $this->getDefaultQuestions();
      foreach ($defaultQuestions as $question) {

        $existQuestion = $this->getExistQuestionInSurvey($questionList, $question);
        if($existQuestion !== null) {
            $surveyQuestion = $existQuestion;
        }else{
            $surveyQuestion=new SurveyQuestion();
            $surveyQuestion->setSurvey($survey);
        }

        $surveyQuestion->setText($question['text']);
        $surveyQuestion->setDescription($question['description']);
        $surveyQuestion->setType($question['type']);
        $surveyQuestion->setCanSkip($question['can_skip']);
        $surveyQuestion->setParams($question['params']);
        $surveyQuestion->setPosition($question['position']);

        $manager->persist($surveyQuestion);
      }

      //delete existing questions that are not in the default questions
      foreach ($questionList as $existQuestion) {
        if($this->existInDefaultQuestions($defaultQuestions, $existQuestion) === false) {
            print("deleting question ".  $existQuestion->getId(). "\r\n");
            $manager->remove($existQuestion);
        }
      }

      $manager->flush();
    }
}
