<?php

namespace tests\AppBundle\Repository;

use App\Entity\{
  Survey,
  SurveyQuestion,
  SurveyResult,
  SurveyResultAnswers
};
use App\Repository\{
  SurveyQuestionRepository,
  SurveyRepository,
  SurveyResultAnswersRepository,
  SurveyResultRepository
};
use PHPUnit\Framework\TestCase;
use Doctrine\Persistence\ObjectManager;
use Doctrine\Common\Collections\Collection;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;
use DateTime;

class EntitiesSaveTest extends KernelTestCase
{
    /**
     * @var \Doctrine\ORM\EntityManager
     */
    private $entityManager;

    protected function setUp(): void
    {
        $kernel = self::bootKernel();
        $this->entityManager = $kernel->getContainer()
            ->get('doctrine')
            ->getManager();
    }

    // protected function tearDown(): void
    // {
    //     parent::tearDown();
    //     // doing this is recommended to avoid memory leaks
    //     $this->entityManager->close();
    //     $this->entityManager = null;
    // }


    public function testSave()
    {

      //create Survey object
      $survey = new Survey();
      $survey->setTitle("Тестовый опрос");
      $survey->setStatus(1);
      $survey->setIsRequired(1);
      $survey->setRepeatDays(31);
      $this->entityManager->persist($survey);
      $this->entityManager->flush();

      $surveyId = $survey->getId();

      $surveyFromDB = $this->entityManager->getRepository(Survey::class)->find($surveyId);
      $this->assertInstanceOf(Survey::class, $surveyFromDB);
      $this->assertEquals($surveyId, $surveyFromDB->getId());

      //create SurveyQuestion object
      $question1 = new SurveyQuestion();
      $question1->setSurvey($survey);
      $question1->setText("question text");
      $question1->setType(1);
      $question1->setCanSkip(1);
      $question1->setParams(["param_1" => "value_1"]);
      $question1->setPosition(1);
      $this->entityManager->persist($question1);
      $this->entityManager->flush();
      $question_1_Id = $question1->getId();
      $question_1_FromDB = $this->entityManager->getRepository(SurveyQuestion::class)->find($question_1_Id);
      $this->assertInstanceOf(SurveyQuestion::class, $question_1_FromDB);
      $this->assertEquals($question_1_Id, $question_1_FromDB->getId());

      //create SurveyQuestion object #2
      $question2 = new SurveyQuestion();
      $question2->setSurvey($survey);
      $question2->setText("question text 2");
      //$question2->setType(2);
      // $question2->setCanSkip(0);
      $question2->setParams(["param_2" => "value_2"]);
      $question2->setPosition(2);
      $this->entityManager->persist($question2);
      $this->entityManager->flush();
      $question_2_Id = $question2->getId();
      $question_2_FromDB = $this->entityManager->getRepository(SurveyQuestion::class)->find($question_2_Id);
      $this->assertInstanceOf(SurveyQuestion::class, $question_2_FromDB);
      $this->assertEquals($question_2_Id, $question_2_FromDB->getId());

      //create SurveyResult object
      $surveyResult = new SurveyResult();
      $surveyResult->setSurvey($survey);
      $surveyResult->setCustomerId(1);
      $surveyResult->setSurveyDateTime(new DateTime());
      $surveyResult->setStatus(1);
      $surveyResult->setProcessingDateTime(new DateTime());
      $this->entityManager->persist($surveyResult);
      $this->entityManager->flush();
      $surveyResultId = $surveyResult->getId();
      $surveyResultFromDB = $this->entityManager->getRepository(SurveyResult::class)->find($surveyResultId);
      $this->assertInstanceOf(SurveyResult::class, $surveyResultFromDB);
      $this->assertEquals($surveyResultId, $surveyResultFromDB->getId());


      //create SurveyResult object #1
      $answer1 = new SurveyResultAnswers();
      $answer1->setSurveyResult($surveyResult);
      $answer1->setQuestion($question1);
      $this->entityManager->persist($answer1);
      $this->entityManager->flush();
      $answer_1_Id = $answer1->getId();
      $answer_1_FromDB = $this->entityManager->getRepository(SurveyResultAnswers::class)->find($answer_1_Id);
      $this->assertInstanceOf(SurveyResultAnswers::class, $answer_1_FromDB);
      $this->assertEquals($answer_1_Id, $answer_1_FromDB->getId());

      //create SurveyResult object #2
      $answer2 = new SurveyResultAnswers();
      $answer2->setSurveyResult($surveyResult);
      $answer2->setQuestion($question2);
      $this->entityManager->persist($answer2);
      $this->entityManager->flush();
      $answer_2_Id = $answer2->getId();
      $answer_2_FromDB = $this->entityManager->getRepository(SurveyResultAnswers::class)->find($answer_2_Id);
      $this->assertInstanceOf(SurveyResultAnswers::class, $answer_2_FromDB);
      $this->assertEquals($answer_2_Id, $answer_2_FromDB->getId());

      $this->entityManager->detach($surveyResult);

      $surveyResult = $this->entityManager->getRepository(SurveyResult::class)->find($surveyResultId);

      //check collection answers
      $answers = $surveyResult->getSurveyResultAnswers();
      $this->assertCount(2, $answers);
      $this->assertInstanceOf(SurveyResultAnswers::class, $answers[0]);

    }


}
